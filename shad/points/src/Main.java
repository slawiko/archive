import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class Point implements Comparable {
	public String point;
	public int distance;

	public Point(String point, int distance) {
		this.point = point;
		this.distance = distance;
	}

	@Override
	public int compareTo(Object o) {
		Point temp = (Point)o;
		return this.distance - temp.distance;
	}
}

public class Main {
	private static int n;

	public static void main(String[] args) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File("input.txt"));
		try {
			n = scanner.nextInt();
		} catch (Exception e) {

		}
		List<Point> points = new ArrayList<>();

		for (int i = 0; i < n; i++) {
			int x = scanner.nextInt(), y = scanner.nextInt();
			int dist = (x * x) + (y * y);
			String temp = x + " " + y;
			points.add(new Point(temp, dist));
		}

		Collections.sort(points);

		for (int i = 0; i < points.size(); i++) {
			System.out.println(points.get(i).point);
		}
	}
}
