import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    private static final int MIN_LENGTH = 8;
    private static final int LEFT_BIG_LETTERS = 65;
    private static final int RIGHT_BIG_LETTERS = 90;
    private static final int LEFT_SMALL_LETTERS = 97;
    private static final int RIGHT_SMALL_LETTERS = 122;
    private static final int LEFT_NUMBERS = 48;
    private static final int RIGHT_NUMBERS = 57;
    private static final String NO = "no";
    private static final String YES = "yes";
    private static final int LOW_LET = 0;
    private static final int BIG_LET = 1;
    private static final int NUM = 2;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("input.txt"));
        String password = null;
        try {
            password = scanner.nextLine();
        } catch (Exception e) {
            System.out.println(NO);
            return;
        }
        boolean [] arr = new boolean[3];
        for (int i = 0; i < 3; i++) {
            arr[i] = false;
        }

        if (password.length() < MIN_LENGTH) {
            System.out.println(NO);
            return;
        }

        for (int i = 0; i < password.length(); i++) {
            if (!arr[LOW_LET] && (password.codePointAt(i) >= LEFT_BIG_LETTERS) && (password.codePointAt(i) <= RIGHT_BIG_LETTERS)) {
                arr[LOW_LET] = true;
            } else if (!arr[BIG_LET] && (password.codePointAt(i) >= LEFT_SMALL_LETTERS) && (password.codePointAt(i) <= RIGHT_SMALL_LETTERS)) {
                arr[BIG_LET] = true;
            } else if (!arr[NUM] && (password.codePointAt(i) >= LEFT_NUMBERS) && (password.codePointAt(i) <= RIGHT_NUMBERS)) {
                arr[NUM] = true;
            }
            if (arr[LOW_LET] && arr[BIG_LET] && arr[NUM]) {
                System.out.println(YES);
                return;
            }
        }

        System.out.println(NO);
    }
}
