#include <windows.h>
#include <iostream>
#include <fstream>

using namespace std;

struct vectorMul {

	int number;
	int n;
	int* vector;
	int* string;
	int* result;

	vectorMul() {
		this->n = 0;
	}

	vectorMul(int n, int number, int* vector, int* string, int* result) {
		this->n = n;
		this->number = number;
		this->vector = vector;
		this->string = string;
		this->result = result;
	}
};

DWORD WINAPI mul(void* _str) {
	vectorMul* str = (vectorMul*)_str;

	int temp = 0;

	for (int i = 0; i < str->n; i++) {
		temp += str->string[i] * str->vector[i];
	}

	//Sleep(1500-500 * str->number);
	Sleep(7);

	str->result[str->number] = temp;
	printf("multiplication of string %i and vector: %i", str->number + 1, temp);
	cout << endl;
	return 0;
}

int main() {
	fstream fin("input.txt");

	int n = 0;
	fin >> n;

	int* vector = new int[n];
	int* result = new int[n];
	int** matrix = new int*[n];

	vectorMul* str = new vectorMul[n];

	for (int i = 0; i < n; i++) {
		fin >> vector[i];
	}

	for (int i = 0; i < n; i++) {
		matrix[i] = new int[n];
		for (int j = 0; j < n; j++) {
			fin >> matrix[i][j];
		}
	}

	HANDLE* hThread = new HANDLE[n];
	DWORD* IDThread = new DWORD[n];

	for (int i = 0; i < n; i++) {
		str[i] = vectorMul(n, i, vector, matrix[i], result);
		hThread[i] = CreateThread(NULL, 0, mul, (void*)&str[i], 0, &IDThread[i]);
	}

	WaitForMultipleObjects(n, hThread, TRUE, INFINITE);

	for (int i = 0; i < n; i++) {
		CloseHandle(hThread[i]);
		cout << str[i].result[i] << " ";
	}

	return 0;
}