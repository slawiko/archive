#include <windows.h>
#include <iostream>

using namespace std;

CRITICAL_SECTION cs;
HANDLE eventStart;
HANDLE* eventPause;
HANDLE** eventNext;
int number;
int usedNum, *usedClosed;

struct Comp {
	int* arr;
	int index;
	int n;
};

DWORD WINAPI marker(LPVOID comp_) {
	Comp* comp = (Comp*)comp_;
	int markedNumber = 0;
	bool* markedElements = new bool[comp->n];

	for (int i = 0; i < comp->n; i++) {
		markedElements[i] = false;
	}
	WaitForSingleObject(eventStart, INFINITE);
	srand(comp->index);

	while (true) {

		int temp = rand() % comp->n;

		EnterCriticalSection(&cs);
		if (comp->arr[temp] == 0) {

			Sleep(5);
			comp->arr[temp] = comp->index;
			Sleep(5);
			markedElements[temp] = true;
			markedNumber++;
			LeaveCriticalSection(&cs);
		}
		else {
			cout << "order: " << comp->index << ", marked elems: " << markedNumber << ", index that can't be filled: " << temp << endl;
			LeaveCriticalSection(&cs);
			SetEvent(eventPause[comp->index - 1]);

			int indicator = WaitForMultipleObjects(2, eventNext[comp->index - 1], FALSE, INFINITE);
			if (indicator == 0) {
				for (int i = 0; i < comp->n; i++) {

					if (markedElements[i]) {
						comp->arr[i] = 0;
					}
				}

				return 0;
			}
			else {
				ResetEvent(eventPause[comp->index - 1]);
			}
		}
	}
}

bool hasUsedBefore(int x)
{
	for (int i = 0; i < usedNum; i++)
	{
		if (x == usedClosed[i])
			return true;
	}
	return false;
}

int main() {
	int	*arr, dimArr;

	cout << "Input dimension: ";
	cin >> dimArr;

	arr = new int[dimArr];
	for (int i = 0; i < dimArr; i++) {
		arr[i] = 0;
	}

	int numberMarkers = 0;
	cout << "Input number of markers: ";
	cin >> numberMarkers;

	HANDLE* hThread = new HANDLE[numberMarkers];
	DWORD* IDThread = new DWORD[numberMarkers];

	usedClosed = new int[numberMarkers];
	usedNum = 0;

	InitializeCriticalSection(&cs);

	eventStart = CreateEvent(NULL, TRUE, FALSE, NULL);
	eventPause = new HANDLE[numberMarkers];
	eventNext = new HANDLE*[numberMarkers];

	for (int i = 0; i < numberMarkers; i++) {
		Comp* comp = new Comp;
		comp->arr = arr;
		comp->index = i + 1;
		comp->n = dimArr;

		eventPause[i] = CreateEvent(NULL, TRUE, FALSE, NULL);
		eventNext[i] = new HANDLE[2];

		for (int j = 0; j < 2; j++) {
			eventNext[i][j] = CreateEvent(NULL, FALSE, FALSE, NULL);
		}

		hThread[i] = CreateThread(NULL, 0, marker, (void*)comp, 0, &IDThread[i]);
	}

	SetEvent(eventStart);

	for (int i = 0; i < numberMarkers; i++) {
		WaitForMultipleObjects(numberMarkers, eventPause, TRUE, INFINITE);
		cout << "Unedited array: " << endl;

		for (int i = 0; i < dimArr; i++) {
			cout << arr[i] << " ";
		}
		cout << endl;

		int numberOfStopedMarker = 0;
		cout << endl << "Input number of marker that you want to stop: ";
		cin >> numberOfStopedMarker;
		cout << endl;

		while (true)
		{
			if (!(numberOfStopedMarker >= 1 && numberOfStopedMarker <= numberMarkers))
			{
				cout << "There is no such number of thread, input the correct one: ";
				cin >> numberOfStopedMarker;
				continue;
			}
			if (hasUsedBefore(numberOfStopedMarker) == true)
			{
				cout << "That thread is already closed, choose another one: ";
				cin >> numberOfStopedMarker;
			}
			else
			{
				usedClosed[usedNum++] = numberOfStopedMarker;
				break;
			}
		}

		SetEvent(eventNext[numberOfStopedMarker - 1][0]);
		WaitForSingleObject(hThread[numberOfStopedMarker - 1], INFINITE);

		cout << "After closing: " << endl;
		for (int i = 0; i < dimArr; i++) {
			cout << arr[i] << " ";
		}
		cout << endl;
		for (int i = 0; i < numberMarkers; i++) {
			SetEvent(eventNext[i][1]);
		}
	}

	return 0;
}
