#include <iostream>
#include <windows.h>
using namespace std;

CRITICAL_SECTION  csQueue;
CRITICAL_SECTION  csOutput;

class SyncQueue {
	HANDLE semaphoreFull;
	HANDLE semaphoreEmpty;
	int size;
	int* queue;
	int maxn;
	int head;
	int tail;
public:
	SyncQueue(int maxn) {
		this->semaphoreFull = CreateSemaphore(NULL, 0, maxn, NULL);
		this->semaphoreEmpty = CreateSemaphore(NULL, maxn, maxn, NULL);
		this->size = 0;
		this->queue = new int[maxn];
		this->maxn = maxn;
		this->head = 0;
		this->tail = 0;
	}

	~SyncQueue() {
		delete [] queue;
	}

	void insert(int element) {
		WaitForSingleObject(semaphoreEmpty, INFINITE);
		EnterCriticalSection(&csQueue);
		queue[tail++] = element;
		tail = tail%maxn;
		size++;
		LeaveCriticalSection(&csQueue);
		ReleaseSemaphore(semaphoreFull, 1, NULL);
	}

	int remove() {
		WaitForSingleObject(semaphoreFull, INFINITE);
		EnterCriticalSection(&csQueue);
		head = head%maxn;
		size--;
		ReleaseSemaphore(semaphoreEmpty, 1, NULL);
		int k = queue[head++];
		LeaveCriticalSection(&csQueue);
		return k;
	}
};

struct parameters {
	SyncQueue* q;
	int num;
	int id;
};

void producer(LPVOID l) {
	parameters* p = (parameters*)l;
	for (int i = 0; i < p->num; i++) {
		p->q->insert(p->id * 100 + i);
		EnterCriticalSection(&csOutput);
		cout << "Producer:  " << p->id * 100 + i;
		cout << endl;
		LeaveCriticalSection(&csOutput);
		Sleep(600);
	}
}

void consumer(LPVOID l) {
	parameters* p = (parameters*)l;
	for (int i = 0; i < p->num; i++) {
		int k = p->q->remove();
		EnterCriticalSection(&csOutput);
		cout << "                         Consumer:  " << k;
		cout << endl;
		LeaveCriticalSection(&csOutput);
		Sleep(300);
	}
}

int main() {
	int n = 0;
	cout << "Enter size of Queue: ";
	cin >> n;

	SyncQueue queue(n);

	int numConsumers, numProduces;
	cout << "Enter the number of Produces ";
	cin >> numProduces;
	cout << "and Consumers : ";
	cin >> numConsumers;

	parameters* pProducer = new parameters[numProduces];
	parameters* pConsumer = new parameters[numConsumers];

	int temp = 0;
	cout << "Enter number of elemets ment to produce for every thread: ";
	for (int i = 0; i<numProduces; i++) {
		cin >> temp;
		pProducer[i].id = i + 1;
		pProducer[i].num = temp;
		pProducer[i].q = &queue;
	}

	cout << "Enter number of elemets ment to consume for every thread: ";

	for (int i = 0; i<numConsumers; i++) {
		cin >> temp;
		pConsumer[i].id = i + 1;
		pConsumer[i].num = temp;
		pConsumer[i].q = &queue;
	}

	InitializeCriticalSection(&csQueue);
	InitializeCriticalSection(&csOutput);

	HANDLE* hThreadProd = new HANDLE[numProduces];
	DWORD* dwThreadProd = new DWORD[numProduces];

	for (int i = 0; i<numProduces; i++) {
		hThreadProd[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)producer, (void*)&pProducer[i], 0, &dwThreadProd[i]);
	}

	HANDLE* hThreadCons = new HANDLE[numConsumers];
	DWORD* dwThreadCons = new DWORD[numConsumers];

	for (int i = 0; i<numConsumers; i++) {
		hThreadCons[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)consumer, (void*)&pConsumer[i], 0, &dwThreadCons[i]);
	}

	WaitForMultipleObjects(numProduces, hThreadProd, TRUE, INFINITE);
	WaitForMultipleObjects(numConsumers, hThreadCons, TRUE, INFINITE);

	DeleteCriticalSection(&csQueue);
	DeleteCriticalSection(&csOutput);

	for (int i = 0; i < numProduces; i++) {
		CloseHandle(hThreadProd[i]);
	}
	for (int i = 0; i<numConsumers; i++) {
		CloseHandle(hThreadCons[i]);
	}
}