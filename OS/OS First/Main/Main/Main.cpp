#include <Windows.h>
#include <fstream>
#include <iostream>

using namespace std;

struct order {

	char name[10];
	int amount;
	double price;
};

int main() {
	char binaryName[200];
	char lpszAppName[200] = "C:\\Users\\Shchaurouski.slava\\Documents\\Visual Studio 2013\\Projects\\OS First\\Creator\\Debug\\Creator.exe ";

	cout << "Input binary file's name: ";
	cin >> binaryName;

	strcat_s(lpszAppName, binaryName);

	STARTUPINFO startupInfoCreator;
	PROCESS_INFORMATION processInformationCreator;

	startupInfoCreator.cb = sizeof(STARTUPINFO);
	ZeroMemory(&startupInfoCreator, startupInfoCreator.cb);

	if (!CreateProcess(NULL, lpszAppName, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &startupInfoCreator, &processInformationCreator)) {
		cout << "fail" << endl;
		return 0;
	}

	WaitForSingleObject(processInformationCreator.hProcess, INFINITE);
	CloseHandle(processInformationCreator.hThread);
	CloseHandle(processInformationCreator.hProcess);

	ifstream fin(binaryName, ios::binary);
	fin.seekg(0, fin.end);
	int size = fin.tellg() / sizeof(order);
	fin.seekg(0, fin.beg);
	order o;

	cout << "Binary file interior:" << endl;
	for (int i = 0; i < size; i++) {
		fin.read((char*)&o, sizeof(order));
		cout << o.name << ' ' << o.amount << ' ' << o.price << endl;
	}

	fin.close();

	char reportName[200];
	char minAmount[9];
	char minGenPrice[9];
	char lpszAppName1[200] = "C:\\Users\\Shchaurouski.slava\\Documents\\Visual Studio 2013\\Projects\\OS First\\Reporter\\Debug\\Reporter.exe ";

	cout << "Input report's name:";	
	cin >> reportName;
	cout << "Input minimal amount: ";
	cin >> minAmount;
	cout << "Input minimal general price: ";
	cin >> minGenPrice;

	strcat_s(lpszAppName1, binaryName);
	strcat_s(lpszAppName1, " ");
	strcat_s(lpszAppName1, reportName);
	strcat_s(lpszAppName1, " ");
	strcat_s(lpszAppName1, minAmount);
	strcat_s(lpszAppName1, " ");
	strcat_s(lpszAppName1, minGenPrice);

	STARTUPINFO startupInfoReporter;
	PROCESS_INFORMATION processInformationReporter;

	startupInfoReporter.cb = sizeof(STARTUPINFO);
	ZeroMemory(&startupInfoReporter, sizeof(STARTUPINFO));
	
	if (!CreateProcess(NULL, lpszAppName1, NULL, NULL, FALSE, CREATE_NEW_CONSOLE, NULL, NULL, &startupInfoReporter, &processInformationReporter)) {
		cout << "fail" << endl;
		return 0;
	}

	WaitForSingleObject(processInformationReporter.hProcess, INFINITE);
	CloseHandle(processInformationReporter.hThread);
	CloseHandle(processInformationReporter.hProcess);

	ifstream fin1(reportName);
	while (!fin1.eof()) {
		char c[200];
		fin1.getline(c, 200);
		cout << c << endl;
	}

	return 0;
}