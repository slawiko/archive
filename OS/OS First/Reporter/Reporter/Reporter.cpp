#include <windows.h>
#include <fstream>

using namespace std;

struct order {

	char name[10];
	int amount;
	double price;
};

int main(int argc, char* argv[]) {
	ifstream fin(argv[1], ios::binary);
	ofstream fout(argv[2]);
	int minAmount = atoi(argv[3]);
	int minGenPrice = atoi(argv[4]);
	order o;

	fout << "File report " << argv[2] << ':' << endl;

	fin.seekg(0, fin.end);
	int size = fin.tellg() / sizeof(order);
	fin.seekg(0, fin.beg);

	for (int i = 0; i < size; i++) {
		fin.read((char*)&o, sizeof(order));
		if (o.amount < minAmount || (o.amount*o.price) < minGenPrice) {
			fout << o.name << ' ' << o.amount << ' ' << o.price << endl;
		}
	}

	fin.close();
	fout.close();

	return 0;
}