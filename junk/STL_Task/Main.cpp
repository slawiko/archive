#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <deque>
#include <list>
#include <queue>
#include <time.h>
#include <algorithm>
#include <numeric>

using namespace std;

bool cmp(int a, int b)
{
	int i=0, j=0;
	while(a)
	{
		a=a/10;
		i++;
	}
	while(b)
	{
		b=b/10;
		j++;
	}
	return (i<j);
}
void zero()
{
	vector<int> v1, v2;
	vector<int>::iterator v1_it, v2_it, v3_it;
// A
	cout << "***A***" << endl;

	int n, m;

	cout << "Input n: ";
	cin >> n;
	cout << "Input m: ";
	cin >> m;

	srand(time(0));
	for(int i=0; i<n; i++)
	{
		int tmp = rand()%100;
		v1.push_back(tmp);
	}
	for(int i=0; i<m; i++)
	{
		int tmp = rand()%100;
		v2.push_back(tmp);
	}

	cout << "v1: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// B
	cout << endl << "***B***" << endl;

	int ind1, ind2;

	cout << "Insesrt index1: ";
	cin >> ind1;
	cout << "Insert index2: ";
	cin >> ind2;

	v1.push_back(100);
	v2.push_back(100);

	v1_it = v1.begin();
	v2_it = v2.begin();

	for(int i=0; i<ind1; i++)
		v1_it++;
	v1.insert(v1_it,10);

	for(int i=0; i<ind2; i++)
		v2_it++;
	v2.insert(v2_it,10);
	
	cout << "v1 with 10 & 100: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2 with 10 & 100: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// C
	cout << endl << "***C***" << endl;

	int rem1, rem2;

	cout << "Input rem1 " << endl;	
	cin >> rem1;
	cout << "Input rem2 " << endl;
	cin >> rem2;

	sort(v1.begin(),v1.end());
	sort(v2.begin(),v2.end());

	v1_it = find(v1.begin(),v1.end(),rem1);
	v2_it = find(v2.begin(),v2.end(),rem2);
	v1.erase(v1_it);
	v2.erase(v2_it);

	cout << "v1 without rem1: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2 without rem2: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// D
	cout << endl << "***D***" << endl;

	int indRem1, indRem2;

	cout << "Input indRem1 " << endl;	
	cin >> indRem1;
	cout << "Input indRem2 " << endl;
	cin >> indRem2;

	v1_it = v1.begin();
	v2_it = v2.begin();

	for(int i=0; i<indRem1; i++)
		v1_it++;
	for(int i=0; i<indRem2; i++)
		v2_it++;

	v1.erase(v1_it);
	v2.erase(v2_it);

	cout << "v1 without indRem1: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2 without indRem2: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// E
	cout << endl << "***E***" << endl;

	int xChange, yChange;
	vector<int>::iterator tmp_it;

	cout << "Input xChange: ";
	cin >> xChange;
	cout << "Input yChange: ";
	cin >> yChange;

	v1_it = find(v1.begin(), v1.end(), xChange);
	v1.insert(v1_it,yChange);
	v1_it = find(v1.begin(), v1.end(), xChange);
	v1.erase(v1_it);


	cout << "v1 change: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;
// F
	cout << endl << "***F***" << endl;

	sort(v1.begin(),v1.end(),cmp);
	sort(v2.begin(),v2.end(),cmp);	

	cout << "v1 sort: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2 sort: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// G
	cout << endl << "***G***" << endl;

	int find1;

	cout << "Input find1: ";
	cin >> find1;

	sort(v1.begin(),v1.end());
	sort(v2.begin(),v2.end());

	v1_it = find(v1.begin(),v1.end(),find1);

	cout << *v1_it << endl;

// H
	cout << endl << "***H***" << endl;

	bool eq;
	
	eq = equal(v1.begin(),v1.end(),v2.begin());
	cout << eq << endl;
// I
	cout << endl << "***I***" << endl;

	vector<int> v3(v1.size()+v2.size());

	merge(v1.begin(),v1.end(),v2.begin(),v2.end(),v3.begin());

	cout << "v3 merge: ";
	for(v3_it = v3.begin(); v3_it < v3.end(); v3_it++)
		cout << *v3_it << ' ';
	cout << endl;
// J
	cout << endl << "***J***" << endl;

	unique(v1.begin(),v1.end());
	unique(v2.begin(),v2.end());

	cout << "v1 unique: ";
	for(v1_it = v1.begin(); v1_it < v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;

	cout << "v2 uniaue: ";
	for(v2_it = v2.begin(); v2_it < v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// K
	cout << endl << "***K***" << endl;

	int sum1, sum2;

	sum1 = accumulate(v1.begin(), v1.end(), 0);
	sum2=accumulate(v2.begin(), v2.end(), 0);

	cout << "Sum1 " << sum1 << endl;
	cout << "Sum2 " << sum2 << endl;
// L
	cout << endl << "***L***" << endl;

	cout << "max_size1: "<< v1.max_size() <<endl;
	cout << "max_size2: "<< v1.max_size() << endl;
	cout << "size1: " << v1.size() << endl;
	cout << "size2: " << v2.size() << endl;
// M
	cout << endl << "***M***" << endl;

	cout << "v1: ";
	for(v1_it=v1.begin(); v1_it<v1.end(); v1_it++)
		cout << *v1_it << ' ';
	cout << endl;
	cout << "v2: ";
	for(v2_it=v2.begin(); v2_it<v2.end(); v2_it++)
		cout << *v2_it << ' ';
	cout << endl;
// N
	cout << endl << "***N***" << endl;

	v1.resize(v1.size()+3);

	cout << "new_size: " << v1.size() << endl;
}
int my_search(stack <double> &a, double b)
{
	deque <double> d;
	d=a._Get_container();
	deque <double> ::iterator it=d.begin(), itd=find (d.begin(), d.end(), b);
	int res=0;
	if (itd!=d.end())
		while(it!=itd)
		{
			res++;
			it++;
		}
	else
		return -1;
	return res;
}
void one()
{
	stack <double> st, st1;
	double r;

	for (int i=1; i<5; i++)
	{
		r=(double)i*4.3;
		st.push(r);
		st1.push(r);
	}
// A
	cout << endl << "***A***" << endl;

	cout << "Input r: ";
	cin >> r;

	st.push(r);
// B
	cout << endl << "***B***" << endl;

	st.pop();
// C
	cout << endl << "***C***" << endl;

	cout << st.top() << endl;
	cout <<"size " << st.size() << endl;
// D
	cout << "***D***" << endl;

	cout << "Input r: ";
	cin >> r;
	cout << my_search(st, r) << endl;
// E
	cout << "***E***" << endl;

	cout <<"size " << st.size() << endl;
// F
	cout << "***F***" << endl;

	if(st.empty())
		cout << "empty\n";
	else
		cout << "not empty\n";
// G
	cout << endl << "***G***" << endl;

	if(st==st1)
		cout << "equal\n";
	if(st!=st1)
		cout << "not equal\n";
}
int my_search2(queue <string> &a, string st)
{
	deque <string> d;
	d=a._Get_container();
	deque <string> ::iterator it=d.begin(), itd=find (d.begin(), d.end(), st);
	int res=0;
	if (itd!=d.end())
		while(it!=itd)
		{
			res++;
			it++;
		}
	else
		return -1;
	return res;
}
void two()
{
	queue <string> q;
	string s;
	cout << "Input elements: " << endl;
	for (int i=0; i<3; i++)
	{
		getline(cin, s);
		q.push(s);
	}
// A
	cout << endl << "***A***" << endl;

	cout << "first - " << q.front() << " last - " << q.back() << endl;
	q.pop();
// B
	cout << endl << "***B***" << endl;

	cout << "Input s: ";
	getline(cin, s);
	cout << my_search2(q, s) << endl;
// C
	cout << endl << "***C***" << endl;

	cout << "size - " << q.size() << endl;
// D
	cout << endl << "***D***" << endl;

	if (q.empty())
		cout << "empty\n";
	else
		cout << "not empty\n";
// E
	cout << endl << "***E***" << endl;

	if (q!=q)
		cout << "not equal\n";
	if (q==q)
		cout << "equal\n";
}
int main()
{
	zero();
	one();
	two();

	return 0;
}