#include <iostream>
#include <fstream>
#include "FormatText.h"

using namespace std;

int main ()
{
	int l=0;
	cout << "What is the length of your string? " << endl;
	cin >> l;

	Text T(l);
	T.FormatText("input.txt");

	return 0;
}