#include "ListStack.h"
#include <iostream>

using namespace std;

ListNode::ListNode()
{	
	strcpy (x, "AAA");
	next=NULL;
}
ListNode::ListNode(char* x1)
{	
	strcpy (x, x1);
	next=NULL;
}
ListNode::ListNode(char* x1, ListNode *next1)
{	
	strcpy (x, x1);
	next=next1;
}
ListStack::ListStack()
{	
	head=NULL;
}
ListStack::ListStack(ListStack& A)
{	
	ListStack sup;
	char[30] x;
	while(!A.IsEmpty())
	{	x=A.pop();
		sup.push(x);
	}
	head=NULL;
	while(!sup.IsEmpty())
	{	int c=sup.pop();
		push(c);
		A.push(c);
	}
}
ListStack::~ListStack()
{	while(!IsEmpty())
		pop();
}
void ListStack::push(int& a)
{	ListNode* s = new ListNode(a,head);
	head=s;
}
int ListStack::pop()
{	if (!IsEmpty())
	{	int sup=head->x;
		ListNode*p=head;
		head=head->next;
		delete p;
		return sup;
	}
	else
		throw EMPTY();
}
bool ListStack::IsEmpty() const
{	return !(head);
}
bool ListStack::IsFull() const 
{	return false;
}