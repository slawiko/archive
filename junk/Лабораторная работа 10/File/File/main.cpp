#include <iostream>
#include <fstream>
#include <conio.h>
#include "File.h"

using namespace std;

int main ()
{
	BinStud A("File1"),B("File2"),Un("File3"),In("File4"),Dr("File5");
	A.writeFile("1.txt");
	B.writeFile("2.txt");
	Union(A,B,Un);
	Intersec(A,B,In);
	Diff(A,B,Dr);
	cout << "First File: " << endl << A << endl;
	cout << "Second File: " << endl << B << endl;
	cout << "File's unity: " << endl << Un << endl;
	cout << "File's intersection: " << endl << In << endl;
	cout << "FIle's difference: " << endl << Dr << endl;

	getch();
	return 0;
}