#include <iostream>
#include <fstream>
#include "File.h"

using namespace std;

BinStud::BinStud(char* fname1)
{
	strcpy(fname, fname1);
	count = 0;
}
void BinStud::writeFile(char* tf)
{
	Student x;

	ifstream fin (tf);
	ofstream fout (fname,ios::binary);

	char c[2];

	while (!fin.eof())
	{
		fin.getline(x.name,20);
		fin >> x.num >> x.group >> x.grade;
		fin.getline(c,2);
		fout.write((char*)&x, sizeof(Student));
	}
}
void Union(BinStud &BS1, BinStud &BS2, BinStud &Res)
{
	Student x, y;

	ifstream f1(BS1.fname, ios::binary);
	if (f1.bad()) return;

	ifstream f2(BS2.fname, ios::binary);
	if (f2.bad()) return;

	ofstream f3(Res.fname, ios::binary);
	if (f3.bad()) return;

	f1.seekg(0,ios::end);
	BS1.count = f1.tellg() / sizeof(Student);
	f1.seekg(0,ios::beg);

	f2.seekg(0,ios::end);
	BS2.count = f2.tellg() / sizeof(Student);
	f2.seekg(0,ios::beg);

	int i=0, j=0;
	
	f1.read((char*)&x, sizeof(Student));
	f2.read((char*)&y, sizeof(Student));

	while (i < BS1.count && j < BS2.count)
	{
		if (x.num == y.num)
		{	
			f3.write((char*)&x, sizeof(Student));

			i++; j++;

			f1.read((char*)&x, sizeof(Student));
			f2.read((char*)&y, sizeof(Student));
		}
		else if (x.num < y.num)
		{
			f3.write((char*)&x, sizeof(Student));
			i++;
			f1.read((char*)&x, sizeof(Student));
		}
		else
		{
			f3.write((char*)&y, sizeof(Student));
			j++;
			f2.read((char*)&y, sizeof(Student));
		}
	}

	while(i<BS1.count)
	{
		f3.write((char*)&x, sizeof(Student));
		f1.read((char*)&x, sizeof(Student));
		i++;
	}
	while(j<BS2.count)
	{
		f3.write((char*)&y, sizeof(Student));
		f2.read((char*)&y, sizeof(Student));
		j++;
	}

	f3.seekp(0, ios::end);
	Res.count = f3.tellp()/sizeof(Student);
}
void Intersec(BinStud &BS1, BinStud &BS2, BinStud &Res)
{
	ifstream f1(BS1.fname, ios::binary);
	if (f1.bad()) return;

	ifstream f2(BS2.fname, ios::binary);
	if (f2.bad()) return;

	ofstream f3(Res.fname, ios::binary);
	if (f3.bad()) return;

	int i=0, j=0;

	Student x, y;

	f1.seekg(0,ios::end);
	BS1.count = f1.tellg() / sizeof(Student);
	f1.seekg(0,ios::beg);

	f2.seekg(0,ios::end);
	BS2.count = f2.tellg() / sizeof(Student);
	f2.seekg(0,ios::beg);

	f1.read((char*)&x, sizeof(Student));
	f2.read((char*)&y, sizeof(Student));

	while (i < BS1.count && j < BS2.count)
	{
		if (x.num == y.num)
		{	
			f3.write((char*)&x, sizeof(Student));

			i++; j++;

			f1.read((char*)&x, sizeof(Student));
			f2.read((char*)&y, sizeof(Student));
		}
		else if (x.num < y.num)
		{
			i++;
			f1.read((char*)&x, sizeof(Student));
		}
		else
		{
			j++;
			f2.read((char*)&y, sizeof(Student));
		}
	}

	f3.seekp(0, ios::end);
	Res.count = f3.tellp()/sizeof(Student);
}
void Diff(BinStud &BS1, BinStud &BS2, BinStud &Res)
{
	ifstream f1(BS1.fname, ios::binary);
	if (f1.bad()) return;

	ifstream f2(BS2.fname, ios::binary);
	if (f2.bad()) return;

	ofstream f3(Res.fname, ios::binary);
	if (f3.bad()) return;

	int i=0, j=0;

	Student x, y;

	f1.seekg(0,ios::end);
	BS1.count = f1.tellg() / sizeof(Student);
	f1.seekg(0,ios::beg);

	f2.seekg(0,ios::end);
	BS2.count = f2.tellg() / sizeof(Student);
	f2.seekg(0,ios::beg);

	f1.read((char*)&x, sizeof(Student));
	f2.read((char*)&y, sizeof(Student));

	while (i < BS1.count && j < BS2.count)
	{
		if (x.num == y.num)
		{
			i++; j++;

			f1.read((char*)&x, sizeof(Student));
			f2.read((char*)&y, sizeof(Student));
		}
		else if (x.num < y.num)
		{
			f3.write((char*)&x, sizeof(Student));
			i++;
			f1.read((char*)&x, sizeof(Student));
		}
		else
		{
			j++;
			f2.read((char*)&y, sizeof(Student));
		}
	}

	while(i<BS1.count)
	{
		f3.write((char*)&x, sizeof(Student));
		f1.read((char*)&x, sizeof(Student));
		i++;
	}

	f3.seekp(0, ios::end);
	Res.count = f3.tellp()/sizeof(Student);
}
ostream & operator << (ostream& out, BinStud& BS)
{
	Student X;
	ifstream fin(BS.fname, ios::binary);
	char c[2];
	for(int i=0; i<BS.count; i++)
	{
		fin.read((char*)&X, sizeof(Student));
		for(int i=0; X.name[i]!='\0'; i++)
			out << X.name[i];
		out << endl << X.num << ' ' << X.group << ' ' << X.grade << endl << endl;
	}
	return out;
}