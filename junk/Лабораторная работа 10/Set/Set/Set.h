#ifndef SSSS
#define SSSS

#include <iostream>

using namespace std;

int cmp(const void *a, const void *b);

struct NOPE{};

class Set
{	
	int * mass;
	int n;    // ���������� ���������
	int size; // ����������� 
	void sort();
public:
	Set();
	Set(int _size);
	Set(const Set &S);
	virtual ~Set();
	void print();
	void add (int a);
	void erase (int a);
	Set & operator= (const Set & S);
	int search (const int a);     // � - �������
	friend Set Union (const Set &A, const Set &B); // �����������
	friend Set Intersec (const Set &A, const Set &B); // �����������
	friend Set Diff (const Set &A, const Set &B); // �������� A ��� B
	friend Set SimmDiff (const Set &A, const Set &B);// ����������� ��� �����������
	friend istream& operator >> (istream& in, Set& S);
	friend ostream& operator << (ostream& out, Set& S);
};
#endif