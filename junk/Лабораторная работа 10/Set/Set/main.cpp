#include "Set.h"
#include <iostream>
#include <conio.h>

using namespace std;

int main ()
{
	int x;

	Set S1, S2, U, I, D, SD;

	cin >> S1;
	cin >> S2;
	U = Union(S1, S2);
	I = Intersec(S1, S2);
	D = Diff(S1, S2);
	SD = SimmDiff(S1,S2);

	cout << "First set: " << S1;
	cout << "Second set: " << S2;
	cout << "Union: " << U;
	cout << "Intersec: " << I;
	cout << "Diff: " << D;
	cout << "SimmDiff: " << SD;

	getch();
	return 0;
}