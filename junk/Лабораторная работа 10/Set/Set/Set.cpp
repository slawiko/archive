#include <iostream>
#include "Set.h"

using namespace std;

int cmp(const void *a, const void *b)
{
	return *(int*)a - *(int*)b;
}
void Set::sort()
{	
	qsort(mass, n, sizeof(int), cmp);
}
Set::Set()
{	
	size = 10;
	n=0;
	mass = new int [size];
}
Set::Set(int _size)
{	
	size = _size;
	n=0;
	mass = new int [size];
}
Set::Set(const Set &S)
{	
	n = S.n;
	size = S.size;
	mass = new int [size];
	for (int i=0; i<n; i++)
		mass[i] = S.mass[i];
}
Set::~Set()
{	
	delete [] mass;
}
void Set::print()
{
	for (int i=0; i<n; i++)
		cout << mass[i] << ' ';
}
void Set::add (int a)
{	
	int x = search(a), *tmp;
	if (x != -1)
		return;
	if (n == size)
	{
		size+=20;
		tmp = new int [size];
		memcpy (tmp, mass, n*sizeof(int));
		delete [] mass;
		mass = tmp;
	}

	int k=0;

	while (mass[k] < a && k < n)
		k++;
	if (k >= n)
	{
		mass[k] = a;
		k++;
	}

	else 
	{
	for (int i=n-1; i >= k; i--)
		mass[i+1] = mass[i];

	mass[k] = a;
	n++;
	}
	
}
void Set::erase (int a)
{
	for (int i=0; i<n; i++)
		if (mass[i] == a)
		{	for (int j=i; j<n-1; j++)
				mass[j] = mass[j+1];
			n--;
			return;
		}

	NOPE ();
	return;
}
Set & Set::operator =(const Set &S)
{
	if (this != &S)
	{	
		n=S.n;
		size = S.size;
		int *tmp = new int [size];

		for (int i=0; i<n; i++)
			tmp[i] = S.mass[i];
		delete mass;
		mass = tmp;
	}
	
	return *this;
}
int Set::search (int a)
{	
	int left_ind=0, mid_ind=0;
	int right_ind = n-1;

	while (left_ind<right_ind)                                      // ���� ������ ��������            
    {   mid_ind = left_ind + (right_ind - left_ind)/2;

		if (a>mass[mid_ind]) 
			left_ind = mid_ind + 1;

		else
			if (a<mass[mid_ind])
				right_ind = mid_ind - 1;
			else
				if (a==mass[mid_ind])
					return mid_ind;
				else
				{	NOPE ();
					return -1;
				}
	}
}
Set Union (const Set &A, const Set &B)
{	
	Set S(A.size + B.size);
	int i=0, j=0, k=0;

	while (i < A.n && j < B.n)
	{	
		if (A.mass[i] == B.mass[j])
		{	
			S.mass[k++] = A.mass[i++];
			j++;
		}
		else
			if (A.mass[i] > B.mass[j])
				S.mass[k++] = B.mass[j++];
			else
				S.mass[k++] = A.mass[i++];
	}

	while (i < A.n)    // ���� �� ���������� �
		S.mass[k++] = A.mass[i++];
	while (j < B.n)	   // ���� �� ���������� �
		S.mass[k++] = B.mass[j++];

	S.n = k;
	return S;
}
Set Intersec (const Set &A, const Set &B)
{
	int size;
	if (A.size >= B.size)
		size = B.size;
	else 
		size = A.size;
	Set S(size);

	int j=0, i=0, k=0;

	while (i < A.n && j < B.n)	
	{	if (A.mass[i] == B.mass[j])
		{	S.mass[k] = A.mass[i++];
			S.n++;
			k++;
		}
		else
			if (A.mass[i] < B.mass[j])
				i++;
			else
				j++;
	}

	return S;
}
Set Diff (const Set &A, const Set &B)
{
	int size;
	if (A.size >= B.size)
		size = A.size;
	else 
		size = B.size;
	Set S(size);

	int i=0, j=0, k=0;

	while (i < A.n && j < B.n)
	{	
		if (A.mass[i] == B.mass[j])
		{	
			i++;
			j++;
		}
		else
			if (A.mass[i] > B.mass[j])
				j++;
			else
				S.mass[k++] = A.mass[i++];
	}

	while (i < A.n)    // ���� �� ���������� �
		S.mass[k++] = A.mass[i++];

	S.n = k;
	return S;
}
Set SimmDiff (const Set &A, const Set &B)
{
	int size;
	if (A.size >= B.size)
		size = A.size;
	else 
		size = B.size;
	Set S(size);

	S = Diff (Union(A,B), Intersec (A,B));

	return S;
}
istream& operator >> (istream& in, Set& S)
{
	in >> S.n;
	if (S.n >= S.size)
	{
		delete [] S.mass;
		S.mass = new int [S.n];
		S.size = S.n;
	}
	
	for (int i=0; i<S.n; i++)
		in >> S.mass[i];

	S.sort();

	return in;
}
ostream& operator << (ostream& out, Set& S)
{
	for (int i=0; i<S.n; i++)
		cout << S.mass[i] << ' ';
	cout << endl;

	return out;
}