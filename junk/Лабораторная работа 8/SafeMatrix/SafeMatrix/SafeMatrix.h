#include <iostream>
#ifndef SSSS
#define SSSS

using namespace std;

// ����������
struct BadDimensions {};
struct BadFirstIndex {};
struct BadSecondIndex {};

// ��������� ������ matrix
class matrix
{	long	n, m;	// ����������� �������
	double* p;		// ��������� �� �������
	matrix();
	class row
	{	long n, m;	// ����������� ������
		double* p;	// ��������� �� ������
	public:
		row(long _m, double* _p);
			// �������� ��������������
		double& operator [] (long j);				// throw BadFirstIndex
		const double& operator [] (long j) const;	// throw BadSecondIndex
	};
public:
		// ������������
	matrix(long _n, long _m);
	matrix(const matrix& M);
		// ����������
	~matrix();
		// �������� ������������
	matrix& operator = (const matrix& M);	// throw BadDimensions
		// �������� ��������������
	row operator [] (long i);				// throw BadFirstIndex
	const row operator [] (long i) const;	// throw BadFirstIndex
		// ������� ���������
	matrix& operator += (const matrix& M);	// throw BadDimensions
	matrix& operator -= (const matrix& M);	// throw BadDimensions
	matrix& operator *= (const matrix& M);	// throw BadDimensions
	matrix& operator *= (const double& d);
		// ������� ���������
	friend matrix operator -(const matrix& M);
	friend matrix operator +(const matrix& M);
		// �������� ���������
	friend matrix operator * (const double& d, const matrix& M);
	friend matrix operator * (const matrix& M, const double& d);
		// ��������� �������� ��������� ����� ��������� ���������� BadDimensions
	friend matrix operator + (const matrix& M1, const matrix& M2);
	friend matrix operator - (const matrix& M1, const matrix& M2);
	friend matrix operator * (const matrix& M1, const matrix& M2);
		// ��������� �����/������
	friend istream& operator>> (istream& in, matrix& M);
	friend ostream& operator<< (ostream& out, const matrix& M);
};
#endif