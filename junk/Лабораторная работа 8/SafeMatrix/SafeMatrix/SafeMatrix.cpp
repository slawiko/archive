#include "SafeMatrix.h"
#include <iostream>

using namespace std;

matrix::row::row (long _m, double* _p)
{	m=_m;
	p=_p;
}
double& matrix::row::operator [] (long j)
{	if ((j<0) || (j>m))
		throw BadSecondIndex();
	return *(p+j);
}
const double& matrix::row::operator [] (long j) const
{	if ((j<0) || (j>m))
		throw BadSecondIndex();
	return *(p+j);
}
matrix::matrix(long _n, long _m)
{	n=_n;
	m=_m;
	p = new double [n*m];
}
matrix::matrix(const matrix& M)
{	n=M.n;
	m=M.m;
	p = new double [n*m];
	for (int i=0; i<n*m; i++)
		p[i]=M.p[i];
}
matrix::~matrix()
{	delete [] p;
}
matrix& matrix::operator = (const matrix& M)
{	if (n!=M.n || m!=M.m)
	{	throw BadDimensions();
	}
	else
		for (int i=0; i<M.n*M.m; i++)
			p[i]=M.p[i];
	return *this;
}
matrix::row matrix::operator [](long i)
{	if ((i<0) || (i>m))
		throw BadFirstIndex();
	return row(m,p+m*i);
}
const matrix::row matrix::operator [] (long i) const
{	if ((i<0) || (i>m))
		throw BadFirstIndex();
	return row(m,p+m*i);
}
matrix& matrix::operator+= (const matrix& M)
{	if (n==M.n && m==M.m)	
		for (int i=0; i<n*m; i++)
			p[i]+=M.p[i];
	else
		throw BadDimensions();
	return	*this;
}
matrix& matrix::operator-= (const matrix& M)
{	if (n==M.n && m==M.m)
		for (int i=0; i<n*m; i++)
			p[i]-=M.p[i];
	else
		throw BadDimensions();
	return *this;
}
matrix& matrix::operator*= (const matrix& M)
{	matrix M1(*this);
	matrix Res(n, M.m);
	double x;
	if (m==M.n)
	{	for (int i=0; i<M1.n; i++)
			for (int j=0; j<M.m; j++)
			{	Res[i][j]=0;
				for (int k=0; k<m; k++)
				{	x=M1[i][k]*M[k][j];
					Res[i][j]+=x;
				}
			}
	}
	else
		throw BadDimensions();
	delete [] p;
	p = new double [Res.n*Res.m];
	n=Res.n;
	m=Res.m;
	*this=Res;
	return *this;
}
matrix& matrix::operator*= (const double &d)
{	for (int i=0; i<n*m; i++)
		p[i]*=d;
	return *this;
}
matrix operator- (const matrix& M)
{	matrix M1(M.n, M.m);
	for (int i=0; i<M1.n; i++)
		for (int j=0; j<M1.m; j++)
			M1[i][j]=-M[i][j];
	return M1;
}
matrix operator+ (const matrix& M)
{	matrix M1(M.n, M.m);
	return M1;
}
matrix operator* (const double& d, const matrix& M)
{	matrix M1(M.n, M.m);
	for (int i=0; i<M.n*M.m; i++)
		M1.p[i]=M.p[i]*d;
	return M1;
}
matrix operator* (const matrix& M, const double& d)
{	matrix M1(M.n, M.m);
	for (int i=0; i<M.n*M.m; i++)
		M1.p[i]=M.p[i]*d;
	return M1;
}
matrix operator+ (const matrix& M1, const matrix& M2)
{	matrix M(M1.n, M1.m);
	if (M1.n==M2.n && M1.m==M2.m)
	{	for (int i=0; i<M.n; i++)
			for (int j=0; j<M.m; j++)
			{	M[i][j]=M1[i][j]+M2[i][j];
			}
	}
	else
		throw BadDimensions();
	return M;
}
matrix operator- (const matrix& M1, const matrix& M2)
{	matrix M(M1.n, M1.m);
	if (M1.n==M2.n && M1.m==M2.m)
	{	matrix M(M1.n,M1.m);
		for (int i=0; i<M.n*M.m; i++)
		{	M.p[i]=M1.p[i]-M2.p[i];
		}
	}
	else
		throw BadDimensions();
	return M;
}
matrix operator* (const matrix& M1, const matrix& M2)
{	matrix M(M1);
	M*=M2;
	return M;
}
istream& operator>> (istream& in, matrix& M)
{	for (int i=0; i<M.n; i++)
		for (int j=0; j<M.m; j++)
			in >> M[i][j];
	return in;
}
ostream& operator<< (ostream& out, const matrix& M)
{	for (int i=0; i<M.n; i++)
	{	for (int j=0; j<M.m; j++)
			out << M[i][j] << ' ';
		out << endl;
	}
	return out;
}