#include <iostream>
#ifndef SSSS
#define SSSS

using namespace std;

// ��������� ������ matrix

struct Bad_dimensions {};

class matrix
{	long n, m;	// ����������� �������
	double*	p;		// ��������� �� �������
	matrix();		// �� ��������� ������� ��������� ������
public:
		// ������������
	matrix(long _n, long _m);
	matrix(const matrix& M);
		// ����������
	~matrix();
		// �������� ������������
	matrix& operator = (const matrix& M);	// throw Bad_dimensions
		// �������� ��������������
	double* operator [] (long i);
	const double* operator [] (long i) const;
		// ������� ���������
	matrix& operator += (const matrix& M);	// throw Bad_dimensions
	matrix& operator -= (const matrix& M);	// throw Bad_dimensions
	//matrix& operator *= (const matrix& M);//!	// throw Bad_dimensions
	matrix& operator *= (const double& d);
		// ������� ���������
	friend matrix operator -(const matrix& M);
	//friend matrix operator +(const matrix& M);//!
		// �������� ���������
	//friend matrix operator * (const double& d, const matrix& M);//!
	//friend matrix operator * (const matrix& M, const double& d);//!
		// ��������� �������� ��������� 
		// ����� ��������� ���������� Bad_dimensions
	friend matrix operator + (const matrix& M1, const matrix& M2);	 
	friend matrix operator - (const matrix& M1, const matrix& M2);
	//friend matrix operator * (const matrix& M1, const matrix& M2);//!
		// ��������� ������/�����
	friend istream& operator>> (istream& in, matrix& M);
	friend ostream& operator<< (ostream& out, const matrix& M);
};
#endif