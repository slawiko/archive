#include "Matrix.h"
#include <iostream>

using namespace std;


matrix::matrix(long _n, long _m)
{	n=_n;
	m=_m;
	p = new double (n*m);
}
matrix::matrix(const matrix& M)
{	n=M.n;
	m=M.m;
	for (int i=0; i<n*m; i++)
		p[i]=M.p[i];
}
matrix::~matrix()
{	delete p;
}
matrix& matrix::operator = (const matrix& M)
{	if (n!=M.n || m!=M.m)
	{	throw BadDimensions();
	}
	else
		for (int i=0; i<M.n*M.m; i++)
			p[i]=M.p[i];
	return *this;
}
double* matrix::operator [](long i)
{	return &p[i*m];
}
const double* matrix::operator [] (long i) const
{	return &p[i*m];
}
matrix& matrix::operator+= (const matrix& M)
{	if (n==M.n && m==M.m)	
		for (int i=0; i<n*m; i++)
			p[i]+=M.p[i];
	else
		throw Bad_dimensions();
	return	*this;
}
matrix& matrix::operator-= (const matrix& M)
{	if (n==M.n && m==M.m)
		for (int i=0; i<n*m; i++)
			p[i]-=M.p[i];
	else
		throw Bad_dimensions();
	return *this;
}
//matrix& matrix::operator*= (const matrix& M)
//{	if (n==M.n && m==M.m)
//	{	matrix M1(n, m);
//		
//	}
//	else
//		throw Bad_dimensions();
//	return M1;
//}
matrix& matrix::operator *=(const double &d)
{	for (int i=0; i<n*m; i++)
		p[i]*=d;
	return *this;
}
matrix operator- (const matrix& M)
{	matrix M1(M.n, M.m);
	for (int i=0; i<M.n*M.m; i++)
		M1.p[i]=-M.p[i];
	return M1;
	
}
//matrix operator+ (const matrix& M)
//{
//}
//matrix operator* (const double& d, const matrix M)
//{
//}
//matrix operator* (const matrix M, const double& d)
//{
//}
matrix operator+ (const matrix& M1, const matrix& M2)
{	matrix M(M1.n, M1.m);
	if (M1.n==M2.n && M1.m==M2.m)
	{	matrix M(M1.n,M1.m);
		for (int i=0; i<M.n*M.m; i++)
		{	M.p[i]=M1.p[i]+M2.p[i];
		}
	}
	else
		throw Bad_dimensions();
	return M;
}
matrix operator- (const matrix& M1, const matrix& M2)
{	matrix M(M1.n, M1.m);
	if (M1.n==M2.n && M1.m==M2.m)
	{	matrix M(M1.n,M1.m);
		for (int i=0; i<M.n*M.m; i++)
		{	M.p[i]=M1.p[i]-M2.p[i];
		}
	}
	else
		throw Bad_dimensions();
	return M;
}
//matrix operator* (const matrix& M1, const matrix& M2)
//{	matrix()
//}
istream& operator>> (istream& in, matrix& M)
{	/*for (int i=0; i<M.n; i++)
		for (int j=0; j<M.m; j++)
			in >> M.p[i][j];*/
	for (int i=0; i<M.n*M.m; i++)
		in >> M.p[i];
	return in;
}
ostream& operator<< (ostream& out, const matrix& M)
{	for (int i=0; i<M.n; i++)
	{	for (int j=0; j<M.m; j++)
			out << M.p[i][j] << ' ';
		out << endl;
	}
	return out;
}