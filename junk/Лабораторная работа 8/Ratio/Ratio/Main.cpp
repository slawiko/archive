#include <iostream>
#include "Ratio.h"

using namespace std;

int main ()
{	ratio R;
	ratio RR;

	try 
	{	cin >> R;
		cin >> RR;
	}
	catch (Zero_divide)
	{	cout << "div 0" << endl;
		return 0;
	}

	cout << "+: " << R+RR << endl;
	cout << "-: " << R-RR << endl;
	cout << "*: " << R*RR << endl;
	cout << "/: " << R/RR << endl;
	if (R>RR) cout << R << ">" << RR << endl; 
	if (R<RR) cout << R << "<" << RR << endl; 
	if (R>=RR) cout << R << ">=" << RR << endl; 
	if (R<=RR) cout << R << "<=" << RR << endl; 
	if (R==RR) cout << R << "==" << RR << endl; 
	if (R!=RR) cout << R << "!=" << RR << endl; 
	
	cout << double(R) << endl;

	return 0;
}