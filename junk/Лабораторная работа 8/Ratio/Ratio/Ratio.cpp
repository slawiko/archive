#include <iostream>
#include <conio.h>
#include <fstream>
#include "Ratio.h"

using namespace std;

ratio::ratio():n(0), d(1){}
void ratio::Reduce()
{	int a=n, b=d, k;
	while (b!=0)
    {	k=a%b;
        a=b;
        b=k;
    }
	while (b) 
	{	a %= b;
		swap (a, b);	//nod=a
    }
	n/=a;
	d/=a;
}
ratio::ratio(long n1, long d1)
{	n=n1;
	d=d1;
}
ratio::ratio(const ratio& r)
{	n=r.n;
	d=r.d;
}
ratio& ratio::operator= (const ratio& r)
{	if (this!=&r)	
	{	n=r.n;
		d=r.d;
	}
	return *this;
}
ratio& ratio::operator+= (const ratio& r)
{	if (r.d!=d)
	{	n=n*r.d + d*r.n;
		d*=r.d;
		Reduce();
	}
	else
	{	n+=r.n;
		Reduce();
	}
	return *this;
}
ratio& ratio::operator-= (const ratio& r)
{	if (d!=r.d)	
	{	n=d*r.n - n*r.d;
		d*=r.d;
	}
	else
		n-=r.n;
	if (n<0)
	{	n=abs(n);
		Reduce();
		n-=2*n;
	}
	else
		Reduce();
	return *this;
}
ratio& ratio::operator*= (const ratio& r)
{	n*=r.n;
	d*=r.d;
	Reduce();
	return *this;
}
ratio& ratio::operator/= (const ratio& r)
{	n*=r.d;
	d*=r.n;
	Reduce ();
	return *this;
}	
ratio::operator double(void) const
{	double x, n1, d1;
	n1=(double)n;
	d1=(double)d;
	x=n1/d1;
	return x;
}
ratio operator+ (const ratio& r)
{	ratio r1(r.n, r.d);
	return r1;
}
ratio operator- (const ratio& r)
{	ratio r1(r.n, r.d);
	Reduce2(r1);
	r1.n-=2*r1.n;
	r1.d-=2*r1.d;
	return r1;
}
bool operator< (const ratio& r1, const ratio& r2)
{	if (r1!=r2)
		if (r1.n*r2.d < r2.n*r1.d)
			return true;
	return false;
}
bool operator> (const ratio& r1, const ratio& r2)
{	if (r1!=r2)
		if (!(r1 < r2))
			return true;
	return false;
}
bool operator== (const ratio& r1, const ratio& r2)
{	if (r1.n==r2.n && r1.d==r2.d)
		return true;
	return false;
}	
bool operator!= (const ratio& r1, const ratio& r2)
{	return !(r1==r2);
}
bool operator<= (const ratio& r1, const ratio& r2)
{	if ((r1==r2) || (r1<r2))
		return true;
	return false;
}
bool operator>= (const ratio& r1, const ratio& r2)
{	if ((r1==r2) || (r1>r2))
		return true;
	return false;
}
ratio operator+ (const ratio& r1, const ratio& r2)
{	ratio r3(r2.n,r2.d);
	r3+=r1;
	Reduce2(r3);
	return r3;
}
ratio operator- (const ratio& r1, const ratio& r2)
{	ratio r3(r2.n,r2.d);
	r3-=r1;
	Reduce2(r3);
	return r3;
}
ratio operator* (const ratio& r1, const ratio& r2)
{	ratio r3(r2.n,r2.d);
	r3.n*=r1.n;
	r3.d*=r1.d;
	Reduce2(r3);
	return r3;
}
ratio operator/ (const ratio& r1, const ratio& r2)
{	ratio r3(r1.n,r1.d);
	r3.n*=r2.d;
	r3.d*=r2.n;
	Reduce2(r3);
	return r3;
}
istream& operator>> (istream& in, ratio& r)
{	in >> r.n >> r.d;
	if (r.d==0) throw Zero_divide();
	return in;
}
ostream& operator<< (ostream& out, const ratio& r)
{	out << r.n << '/' << r.d;
	return out;
}
void Reduce2 (ratio& r)
{	int a=r.n, b=r.d, k;
	while (b!=0)
    {	k=a%b;
        a=b;
        b=k;
    }
	while (b) 
	{	a %= b;
		swap (a, b);	//nod=a
    }
	r.n/=a;
	r.d/=a;
}