#ifndef SSS
#define SSS

#include <iostream>
#include <string>

using namespace std;

struct Treenode
{
	int key;
	string data;
	Treenode *left,*right;
	Treenode():key(0),left(NULL),right(NULL){}
};
class Tree
{
public:
	Treenode* root;
public:
	Tree():root(NULL){}
	Treenode*& FindNode(Treenode*& node, int key)
	{
		if(!node)
			return node;
		if(node->key==key)
			return node;
		else
		if(node->key<key)
			return FindNode(node->right, key);
		else 
			return FindNode(node->left, key);
	}
	void RemoveNodeByKey(int key)
	{
		Treenode*& save=FindNode(root,key);
		if(save)
			RemoveNode(save);
	}

	void RemoveNode(Treenode*& p)
	{
		if(!p->left || !p->right)
		{
			Treenode* save=p;
			p=(p->left)?p->left:p->right;
			delete save;
		}
		else 
		{
			Treenode**p2=&p->right;
			while((*p2)->left)
				p2=&((*p2)->left);
			p->key=(*p2)->key;
			p->data.insert(0,(*p2)->data);
			RemoveNode(*p2);
		}
	}
	bool add(int key, string data)
	{
		Treenode*& p=FindNode(root,key);
		if(p)
			return false;
		else
		{
			p=new Treenode;
			p->key=key;
			p->data.insert(0,data);
			p->left=NULL;
			p->right=NULL;
		}
	}
	void Print(Treenode* T)
	{
		if(T->left)
			Print(T->left);
		cout << T->key << ' ' << T->data << endl;
		if(T->right)
			Print(T->right);
	}
	void Print()
	{
		Print(root);
	}		
};
#endif