#include <iostream>
#include "Queue.h"

using namespace std;

node::node ()
{	x=0;
	y=0;
	next=0;
}
node::node (int x1, int y1, node *next1)
{	x=x1;
	y=y1;
	next=next1;
}
Queue::Queue ()
{	head=NULL;
	tail=NULL;
}

Queue::Queue(const Queue &A)
{	head=tail=NULL;
	node* tmp=A.head;
	while (tmp)
	{	push(tmp->x, tmp->y);
		tmp=tmp->next;
	}
}

Queue::~Queue()
{	while (!is_empty()) del();
}
	void Queue::push (int x1, int y1)
{	if (tail==NULL)
	{	tail=new node(x1, y1, NULL);
		head=tail;
	}
		tail->next=new node(x1, y1, NULL);
	tail=tail->next;	
}
	node* Queue::pop ()
{	node* a=head;
	head=head->next; 
	return a;
}
	void Queue::del()
{	delete head;
	head=head->next; 
}
	bool Queue::is_empty ()
{	if (head==NULL) return 1;
	else return 0;
}
	void Queue::print (ostream &out)
{	node* n=head;
	while(n!=NULL)
	{	out<<n->x<<" "<<n->y<<endl;
		n=n->next;
	}
}