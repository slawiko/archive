#include <iostream>
#include "Graph.h"

using namespace std;

Graph::Graph (int n1, int m1)
{	n=n1;
	m=m1;
	spisok = new Queue[n];
}

void Graph::Input (istream & in)
{	in >> n >> m;
	delete []spisok;
	spisok = new Queue [n];
	int num;
	int x1, y1;
	for (int i=0; i<m; i++)
	{	in >> num >> x1 >> y1;
		spisok[num].push (x1,y1);
	}
}

Graph::~Graph() 
{	delete []spisok;
}