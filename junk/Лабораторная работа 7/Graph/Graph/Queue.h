#include <iostream>
#ifndef SSSSS
#define SSSSS

using namespace std;

struct node
{	int x, y;
	node* next;
public:
	node ();
	node (int x1, int y1, node *next1);
};

class Queue
{	node *head;
	node *tail;
public:
	Queue ();
	Queue(const Queue &A);
	virtual ~Queue();
	void push (int x1, int y1);
	node* pop ();
	void del();
	bool is_empty ();
	void print (ostream &out);
};
#endif