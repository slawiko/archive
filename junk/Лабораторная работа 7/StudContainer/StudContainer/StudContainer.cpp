#include <iostream>
#include <conio.h>
#include <cstring>
#include <fstream>
#include "StudContainer.h"

using namespace std;

int main ()
{	int choice, Size;
	char NameFind[10], NameDel[10];
	Student *find, *del;
	Student s, ins;
	ofstream fout ("Output.txt");

	cout << "Input a number of students ";
	cin >> Size;
	cout << endl;
	char k2[2];
	cin.getline(k2,2);

	StudContainer SQ(Size);

	try
	{	for (int i=0; i<Size; i++)
		{	cout << "Last name of student: ";
			cin >> s.name;
			cout << "Number of group: ";
			cin >> s.num;
			cout  << "Grade: ";
			cin >> s.grade;
			SQ.insert(s);
			char k[2];
			cin.getline(k,2);
			cout << endl;
		}
	}
	catch (FULL)
	{	cout << "Container is full";
	}

	cout << "If you want to find student, press 1" << endl;
	cout << "If you want to report in a file, press 2" << endl;
	cout << "If you want to sort students by name, press 3" << endl;
	cout << "If you want to sort students by group and name, press 4" << endl;
	cout << "If you want to print info, press 5" << endl;
	cout << "If you want to delete Student, press 6" << endl;
	cout << "If you want to insert student, press 7" << endl;
	cout << "If you want to exit, press 8" << endl;
	cin >> choice;
	cout << endl;

	while (choice<9 && choice>0)
	{	switch (choice)
		{	case 1:
				cout << "Input name of nessesary student: ";
				cin >> NameFind;
				if (SQ.findByName(NameFind)) 
				{	find=SQ.findByName(NameFind);
					cout << "Name: " << find->name << ", " << "Number of group: " << find->num << ", " << "Grade: " << find->grade << endl;
					cout << endl;
				}
				else cout << "Nesessary student doesn't exist" << endl;
				break;
			case 2:
				SQ.report(fout);
				break;
			case 3:
				SQ.sortByName();
				break;
			case 4:
				SQ.sortByGroupAndName();
				break;
			case 5:
				SQ.print(cout);
				cout << endl;
				break;
			case 6:
				cout << "Input name of nessesary student: ";
				cin >> NameDel;
				cout << endl;
				if (SQ.del(NameDel));
				else cout << "Nesessary student doesn't exist" << endl << endl;
				break;
			case 7:
				try
				{	cout << "Last name of student: ";
					cin >> ins.name;
					cout << "Number of group: ";
					cin >> ins.num;
					cout  << "Grade: ";
					cin >> ins.grade;
					if (SQ.insert(ins));
					else cout << "This student already exists" << endl << endl;
				}
				catch (FULL)
				{	cout << "Container is full" << endl;
				}
				break;
			case 8:
				return 0;
				break;
		}
		cout << "If you want to find student, press 1" << endl;
		cout << "If you want to report in a file, press 2" << endl;
		cout << "If you want to sort students by name, press 3" << endl;
		cout << "If you want to sort students by group and name, press 4" << endl;
		cout << "If you want to print info, press 5" << endl;
		cout << "If you want to delete Student, press 6" << endl;
		cout << "If you want to insert student, press 7" << endl;
		cout << "If you want to exit, press 8" << endl;
		cin >> choice;
		cout << endl;
	}

	delete find;
	delete del;
	
	getch();
	return 0;
}