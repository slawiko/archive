#include <iostream>
#include <conio.h>
#include <cstring>
#include <fstream>
#ifndef SSSSS
#define SSSSS

using namespace std;

struct FULL{};

struct Student
{	char  name[10];			// ������� ��������
	int   num;				// ����� ������
	double  grade;			// ������� ���
public:
	Student ();
	Student (char *name1, int num1, double grade1);
};

int StudName_cmp (const void *a1, const void *b1);
int StudGroup_cmp (const void *a1, const void *b1);
int StudGroupAndName_cmp (const void *a1, const void *b1);

class StudContainer
{	int size;												// ����������� �������
	Student* p;												// ������ ������� � ���������
	int count;												// ���������� ��������� � ����������
public:
	StudContainer();	                                    // �����������
	StudContainer(int& _n);                                 // ����������� � ����������
	StudContainer(const StudContainer& B);                  // ����������� �����������
	const StudContainer & operator=(const StudContainer &B);// ��������=
	virtual ~StudContainer();								// ����������
	bool insert(const Student& s);						    // ��������� �������� � ���������
	bool del(char* name1);			    				    // ���������� �������� �� ���������
	void sortByName();									    // ���������� ������� �� ������	
	void sortByGroupAndName ();							    // ���������� �� ������ � �����
	void sortByGroup ();									// ���������� �� ������
	Student* findByName(char* name1);					    // ����� �������� �� �����
	void report(ostream& out);							    // ������� ����� � ����
	void print(ostream& out);							    // ����������� ������ ���������
};
#endif