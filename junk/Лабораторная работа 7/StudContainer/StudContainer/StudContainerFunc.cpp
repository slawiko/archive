#include <iostream>
#include <conio.h>
#include <cstring>
#include <fstream>
#include "StudContainer.h"

using namespace std;

Student::Student ()
{	num=0;
	grade=0.;
	strcpy(name,"1111");
}

Student::Student (char *name1, int num1, double grade1)
{	strcpy(name, name1);
	num=num1;
	grade=grade1;
}

int StudName_cmp (const void *a1, const void *b1)
{	Student*a=(Student*)a1;
	Student*b=(Student*)b1;
	return strcmp(a->name, b->name);
}

int StudGroup_cmp (const void *a1, const void *b1)
{	Student*a=(Student*)a1;
	Student*b=(Student*)b1;
	return a->name - b->name;
}

int StudGroupAndName_cmp (const void *a1, const void *b1)
{	Student*a=(Student*)a1;
	Student*b=(Student*)b1;
	if (!(a->num - b->num)) return strcmp(a->name, b->name);
	else return a->num - b->num;
}

StudContainer::StudContainer()		 // �����������
{	size=5;
	count=0;
	p=new Student[size];
}

StudContainer::StudContainer(int& _n)  // ����������� � ����������
{	size=_n;
	count=0;
	p=new Student[size];
}

StudContainer::StudContainer(const StudContainer& B)  // ����������� �����������
{	size=B.size;
	count=B.count;
	p=new Student [size];
	for (int i=0; i<count; i++) p[i]=B.p[i];	
}

const StudContainer & StudContainer::operator=(const StudContainer &B)      //��������=
{	if (this!=&B)
	{	delete [] p;
		size=B.size;
		count=B.count;
		p=new Student [size];
		for (int i=0; i<count; i++) p[i]=B.p[i];
	}
	return *this;
}

StudContainer::~StudContainer()					// ����������
{	delete [] p;
}

bool StudContainer::insert(const Student& s)              // ��������� �������� � ���������
{	if (findByName((char*)s.name)) return NULL;
	if (count<size)
		p[count++]=s;
	else
		throw  FULL();
	return true;	
}

bool StudContainer::del(char* name1)						 // ���������� �������� �� ���������
{	if (findByName(name1))
	for (int i=0; i<count; i++)
	if (!strcmp(p[i].name, name1))
	{	for (int j=i; j<=count; j++) p[j]=p[j+1];
		count--;
		return true;
	}
	return NULL;
}

void StudContainer::sortByName()							  // ���������� ������� �� ������
{	qsort (p, count, sizeof(Student), StudName_cmp);
}
		
void StudContainer::sortByGroupAndName ()					  // ���������� �� ������ � �����
{	qsort (p, count, sizeof(Student), StudGroupAndName_cmp);
}

void StudContainer::sortByGroup ()
{	qsort (p, count, sizeof(Student), StudGroup_cmp);
}

Student* StudContainer::findByName(char* name1)				  // ����� �������� �� �����
{	for (int i=0; i<count; i++)
	{	if (!strcmp(p[i].name, name1))
		{	Student *a = &p[i];
			return a;
		}
	}		
	return NULL;
}

void StudContainer::report(ostream& out)				 // ������� ����� � ����
{	sortByGroup ();
	int sum=p[0].grade;
	int iterator=1;
	for (int j=1; j<=count; j++)
	{	if (p[j].num==p[j-1].num) 
		{	sum+=p[j].grade; 
			iterator++;
		}
		else
		{	out << "Number of group: " << p[j-1].num << ", Grade: " << sum/iterator << endl; 
			sum=p[j].grade;
			iterator=1;
		}
	}
}

void StudContainer::print(ostream& out)              // ����������� ������ ���������
{	for (int j=0; j<count; j++)
	{	out << "Name: ";
		puts (p[j].name);
		out << "Number of group: " << p[j].num << ", Grade: " << p[j].grade << endl;
	}
}