#include <iostream>
#include <conio.h>
#include "Queue.h"

using namespace std;

node::node ()
{	x=0;
	y=0;
	next=0;
}
node::node (int x1, int y1, node *next1)
{	x=x1;
	y=y1;
	next=next1;
}
const node & node::operator= (const node& B)
{	if (this!= &B)
	{	x=B.x;
		y=B.y;
		next=B.next;
	}
	return *this;
}
Queue::Queue ()
{	head=NULL;
	tail=NULL;
}

Queue::Queue(const Queue &A)
{	head=tail=NULL;
	node* tmp=A.head;
	while (tmp)
	{	push(tmp->x, tmp->y);
		tmp=tmp->next;
	}
}

Queue::~Queue()
{	node *n;
	while(!is_empty())
	{	n=head;
		head=head->next;
		delete n;
	}
}
bool Queue::push (int x1, int y1)
{	if (tail==NULL)
	{	tail=new node(x1, y1, NULL);
		head=tail;
		return false;
	}
	tail->next=new node(x1, y1, NULL);
	tail=tail->next;
	return false;
}
node Queue::pop ()
{	node a=*head;
	node* b=head;
	head=head->next;
	if(is_empty())
	{	tail = NULL;
		head = NULL;
	}
	delete b;
	return a;
}
bool Queue::is_empty ()
{	if (head==NULL) return true;
	else return false;
}
void Queue::print (ostream &out)
{	node* n=head;
	while(n!=NULL)
	{	out<<n->x<<" "<<n->y<<endl;
		n=n->next;
	}
}