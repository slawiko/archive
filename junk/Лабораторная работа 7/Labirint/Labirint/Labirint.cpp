#include <iostream>
#include <conio.h>
#include "Queue.h"

using namespace std;

int main ()
{	node hod[4], N, vihod[40];
	ifstream fin("Input.txt");
	char k[20];

	hod[0].x=1;  //������
	hod[0].y=0;  //������

	hod[1].x=-1; //�����
	hod[1].y=0;	 //�����

	hod[2].x=0;	 //�����	
	hod[2].y=-1; //�����

	hod[3].x=0;	 //����
	hod[3].y=1;	 //����

	int n=10, m=15, xs, ys;
	
	char **labirint = new char *[n];
	bool flag = false;	

	for (int i=0; i<n; i++)
	{	labirint[i] = new char [m];
	}
	
	while (!fin.eof())
		for (int i=0; i<n; i++)
		{	fin.getline(k, m+1);
			for (int j=0; j<m; j++)
				labirint[i][j]=k[j];
		}

	for (int i=0; i<n; i++)
	{	for (int j=0; j<m; j++)
			cout << labirint[i][j];
		cout << endl;
	}	
	
	node *exit = new node [m*n];
	Queue Q;

	cout << "Vvedite start\n";
	cin >> xs >> ys;

	Q.push(xs,ys);
	labirint[ys][xs]='4';

	for (int i=0; i<n; i++)
	{	for (int j=0; j<m; j++)
			cout << labirint[i][j];
		cout << endl;
	}	
	
	while (!Q.is_empty())
	{	N = Q.pop();
		if (N.x==0 || N.x==m-1 || N.y==0 || N.y==n-1)
		{	flag=true;
			break;
		}
		for (int i=0; i<4; i++)
		{	int yn, xn;
			yn=N.y + hod[i].y;
			xn=N.x + hod[i].x;
			if (labirint[yn][xn]==' ')
			{	Q.push(xn,yn);
			labirint[yn][xn]=i + '0';
			}
		}
	}

	cout << endl;

	for (int i=0; i<n; i++)
	{	for (int j=0; j<m; j++)
			cout << labirint[i][j];
		cout << endl;
	}	

	if (flag)
		cout << "Escape exists\n";

	if (!flag)
	{	cout << "vihoda net";
		getch();
		return 0;
	}

	int j=0;

	vihod [0] = N;

	while (labirint[N.y][N.x]!='4')
	{	char C = labirint [N.y][N.x];
		int q = C - '0';
		N.y-=hod[q].y;
		N.x-=hod[q].x;
		vihod [++j] = N;
	}

	cout << j << " hodov\n";
	for (int i=j; i>=0; i--)
	{	cout << vihod[i].x << ' ' << vihod[i].y << endl;
	}	

	fin.close();
	getch();
	return 0;
}