/*������� ��� ������ ����� � ������ �� ����� �����. ����������� ������� 
�������� �������� ������, ��� ����� � ����� ������, � ������� ����� �������� 
���������. ������� ���������� ����� ������.*/

#include "iostream"
#include "conio.h"
#include "cstring"

using namespace std;

char* ReplaceWord(char *str, char *Iword, char *Rword)        //Iword - ����������� �����, Rword - �����, ������� ����������, NewStr - ����� ������
{	int lIw, lRw, lDS1;                                                    // lIw - ����� ������������ �����, lRw - ����� �����, ������� ����������, lDS1 - ����� dopstr1                              
	char *p, dopstr1[80], dopstr2[30], *newstr;

	newstr=new char[300];
	
	p=strstr(str, Rword);

	if (p==0) {cout << "This word doesn`t exist in the string\n"; return 0;}
	else 
	{	lIw=strlen(Iword);
		lRw=strlen(Rword);
		strcpy(newstr, str);
	
		while(p)
		{	strcpy(dopstr2, Iword);
			strcat(dopstr2, p+lRw);

			lDS1=strlen(newstr)-strlen(p);

			strncpy(dopstr1, newstr, lDS1);

			dopstr1[lDS1]='\0';

			strcat(dopstr1, dopstr2);
			strcpy(newstr, dopstr1);
			p=strstr(newstr, Rword);
		}
	}

	return newstr;
}
void main ()
{	char str[50], Iword[20], Rword[20], *res;

	cout << "Input string:\n";
	gets (str);
	cout << "Input insert word:\n";
	gets (Iword);
	cout << "Input 'replace word':\n";
	gets (Rword);

	res=ReplaceWord (str, Iword, Rword);

	if(res) puts(res);	

	delete [] res;

	getch ();	
}	