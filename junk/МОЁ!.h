НОД/НОК
#include "iostream"
using namespace std;

void main ()
 {	int r, a, b, c;

 	cin >> a >> b;
	c=a*b;
 
    while (b!=0)
    {	r=a%b;
        a=b;
        b=r;
    }
	/*while (b) 
	{	a %= b;
		swap (a, b);									
    }

	nod=a;
    nok=a1*b1/a;*/

 	c=c/a;

	cout <<'nok' << c <<' nod '<< a;
} 

НОД/НОК асм
_asm
{		mov ebx,a
		mov ecx,b
		cmp ebx,ecx
		JNL if1                // если не меньше
		xchg ebx,ecx             // swap (a,b)

if1:	                    	// a>b
loop1:	mov eax,ebx
		xor edx,edx               // зануляет edx перед деленеием
		idiv ecx
		mov ebx,edx
		cmp ebx,ecx
		JNL if2                  // если не меньше
		xchg ebx,ecx
if2:		                      // a>b
		cmp ebx,0
		jz ex1                  //если 0
		cmp ecx,0
		jz ex1                  //если 0
		jmp loop1
ex1:
		add ebx,ecx
		mov nod,ebx
		mov eax, a
		xor edx,edx
		idiv nod
		imul b
		mov dword ptr nok,eax
		mov dword ptr nok+4,edx
}


Алгоритм бинарного поиска
while (left_ind<right_ind)                                           // цикл поиска элемента            
    {   mid_ind = left_ind + (right_ind - left_ind)/2;

   	    if (x>a[mid_ind]) left_ind = mid_ind+1;
		else
	    if (x<a[mid_ind]) right_ind = mid_ind-1;
		else
		{   cout << "Index of the required element: " << mid_ind; 
		    getch (); 
			return 0;
		}
	}
	
Бинарный поиск
#include "iostream"
#include "conio.h"

using namespace std;
int cmp_values(const void *a, const void *b)                       
{   return *(int*)a - *(int*)b;
}
int main()
{   int *a, s=0, mid_ind=0, left_ind=0, right_ind=0, x=0;
    
    cout << "Input the dimension of the array: ";
	cin >> s;

	cout << "Input the required number: ";
	cin >> x;

	a = (int*)malloc(s*sizeof(int));                                // выделение памяти под динамический массив

    cout << "Input elements of the array \n";
	for (int i=1; i<=s; ++i) cin >> a[i];

	cout << "Your array is: {";
	for (int k=1; k<=s; k++) cout << " " << a[k] ;
	cout << " }\n";
	
	qsort(a, s+1, sizeof(int), cmp_values);                         // сортировка

	cout << "Your sorted array is: {";                              
	for (int k=1; k<=s; k++) cout << " " << a[k] ;
	cout << " }\n";

	left_ind=1;
	right_ind=s;

	if (x>a[right_ind]) 
	{   cout << "The required number isn`t in the array\n";         // проверка на принадлежность интервалу
	    getch (); 
		return 0;
	}

    if (x<a[left_ind]) 
	{   cout << "The required number isn`t in the array\n"; 
	    getch (); 
		return 0;
	}

    while (left_ind<right_ind)                                           // цикл поиска элемента            
    {   mid_ind = left_ind + (right_ind - left_ind)/2;

   	    if (x>a[mid_ind]) left_ind = mid_ind+1;
		else
	    if (x<a[mid_ind]) right_ind = mid_ind-1;
		else
		{   cout << "Index of the required element: " << mid_ind; 
		    getch (); 
			return 0;
		}
	}

	cout << "The required number isn`t in the array\n"; 
	getch (); 
	return 0;	
} 

бинарный поиск ассемблер 
for (int i=0; i<s; i++) cin >> a[i];

	__asm
	{		lea edx,a       //база
			mov ecx,s       //ecx - счетчик i
	loop1:
			mov esi,s       //esi - счетчик j
			sub esi,2      
	loop2:
			mov eax,[edx][esi*4]
			cmp eax,[edx][esi*4+4]
			jng else1       // если не больше
			mov ebx,[edx][esi*4+4]
			mov [edx][esi*4],ebx
			mov [edx][esi*4+4],eax
	else1:
			dec esi
			mov eax,s
			sub eax,ecx
			cmp esi,eax
			jge loop2     // если больше или равно
			
			dec ecx        // есх--
			cmp ecx,0
			jge loop1
	}

quicksort 
int cmp_values(const void *a, const void *b)                       
{   return *(int*)a - *(int*)b;
}
qsort(a, s, sizeof(int), cmp_values);    

Сортировки 
#include "iostream"
#include "conio.h"

using namespace std;

int Sort_Ins (int s, int *a);
int Sort_Sel (int s, int *a);
int Sort_Bub (int s, int *a);
int main ()
{   int *a, s=0, type=0, k=1;

    cout << "Input the dimension of the array: ";
	cin >> s;
	
	cout << "Select the type of sort\nEnter 1 - Insertion sort, 2 - Selection sort, 3 - Bubble sort\n";
	cin >> type;

    while (type>3 || type<1)
    	{   cout << "Reselect the type of sort\nEnter 1 - Insertion sort, 2 - Selection sort, 3 - Bubble sort\n";
	        cin >> type;
	    }

	if (type==1) cout << "Insertion sort\n";
	if (type==2) cout << "Selection sort\n";
	if (type==3) cout << "Bubble sort\n";	

	a = (int*)malloc(s*sizeof(int));                                          // выделение памяти под динамический массив

    cout << "Input elements of the array \n";                                 // ввод элементов массива с консоли
	for (int i=1; i<=s; ++i) cin >> a[i];

	if (type==1) Sort_Ins (s, a);
	if (type==2) Sort_Sel (s, a);                                             // выбор типа сортировки
	if (type==3) Sort_Bub (s, a);	
	
	cout << "Your sorted array is: {";                              
	for (int k=1; k<=s; k++) cout << " " << a[k];                             // вывод отсортированного массива
	cout << " }\n";

	getch();
	return 0;
}
int Sort_Ins (int s, int *a)                                                  // функция сортировки вставками
{   for(int i=1; i<=s; i++)      
    {   for(int j=i; j>0 && a[j-1]>a[j]; j--) swap(a[j-1],a[j]);         
    }
    return *a;
}
int Sort_Sel (int s, int *a)                                                  // функция сортировки выбором
{   int j=1, min=0;
    for (int i=0; i<s-1; i++)
	{   min=i;
		for (j=i+1; j<=s; j++)
		{   if (a[j]<a[min]) min=j;
	    }
		if (min!=i) swap (a[i], a[min]);
	}
	return *a;
}
int Sort_Bub (int s, int *a)                                                  // функция сортировки пузырьком
{   for (int ind=0; ind<s-1; ind++)   
	{   for (int k=1; k<s; k++) 
        {   if (a[k]>a[k+1]) swap (a[k], a[k+1]);	
        }
	}
	return *a;
}

пузырьком 
int Sort_Bub (int s, int *a)                                                  // функция сортировки пузырьком
{   for (int ind=0; ind<s-1; ind++)   
	{   for (int k=1; k<s; k++) 
        {   if (a[k]>a[k+1]) swap (a[k], a[k+1]);	
        }
	}
	return *a;
}
              
выбором
int Sort_Sel (int s, int *a)                                                  // функция сортировки выбором
{   int j=1, min=0;
    for (int i=0; i<s-1; i++)
	{   min=i;
		for (j=i+1; j<=s; j++)
		{   if (a[j]<a[min]) min=j;
	    }
		if (min!=i) swap (a[i], a[min]);
	}
	return *a;
}

вставками 
int Sort_Ins (int s, int *a)                                                  // функция сортировки вставками
{   for(int i=1; i<=s; i++)      
    {   for(int j=i; j>0 && a[j-1]>a[j]; j--) swap(a[j-1],a[j]);         
    }
    return *a;
}	
	
наибольший в столбце и наименьший в строке
#include "iostream"
#include "conio.h"

using namespace std;
int main()
{   int str_col_n=0, str_n=0, col_n=0, **a, j=0, jj=0, i=1, ii=0, min=0, max=0;
    
    cout << "Input size of matrix: ";
    cin >> str_col_n;

	str_n = str_col_n;
	col_n = str_col_n;

    a = new int*[col_n];                                     // выделение памяти под массив указателей	
 
	for (int i=0; i<col_n; i++) 
		a[i] = new int[str_n];                               // выделение памяти под массив значений

	cout << "Input the elements of matrix\n";

	for (i=0; i<col_n; i++)
	{   for (j=0; j<str_n; j++)   
		cin >> a[i][j];
	}

	for (i=0; i<str_col_n; i++)                              // вывод матрицы
	{   for (j=0; j<str_col_n; j++) 
	    {   cout << a[i][j] << " ";
	    }
	    cout << "\n";
	}

    bool flag = false;

	for (j=0; j<str_n; j++)
	{   max=a[0][j];
	    jj=j;
		ii=0;
		for (i=1; i<col_n; i++)
	    {   if (a[i][j]>=max) 
		    {   max=a[i][j];                                     // поиск максимального в столбце
		        ii=i;
		    }		 
	    }

		min=a[ii][j];
		int j2=j;;
		for (jj=0; jj<str_n; jj++)                               // поиск минимального в строке 
		{   
		    if (a[ii][jj]<min)
			{   min=a[ii][jj];
				j2=jj;
			}
		}

		jj=0;

		if (max==min) 
		{   cout << "MinStr & MaxCol " << max<<"["<<ii<<","<<j2<<"]"<<endl;
			flag=true;
		}			
	}

	if (flag==false)	 
	{   cout << "MinStr & MaxCol do not exist";
		getch();
		return 0;
	}

	delete [] a;

	getch();
	return 0;
}

корни квадратного уравнения 
#include "iostream"
#include "conio.h" 
#include "math.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{   int a,b,c;    
 
    cout << "Napominayu: kvadratnoe uravnenie imeet vid ax^2+bx+c=0, gde a,b,c - koefficienti\n";
	cout << "Vvedite koefficient a\n";
	cin >> a;

	while (a==0)
	{    cout << "Vvedite koefficient a, ne ravnii 0\n";
	     cin >> a;
	}

	cout << "Vvedite koefficient b\n";
	cin >> b;

	while (b==0)
	{    cout << "Vvedite koefficient b, ne ravnii 0\n";
	     cin >> b;
	}

    cout << "Vvedite koefficient c\n";
	cin >> c;

	while (c==0)
	{    cout << "Vvedite koefficient c, ne ravnii 0\n";
	     cin >> c;
	}

	double D=0.0,D1=0.0;

	D=b*b-4*a*c; 
	D1=sqrt(D);

    double x=0.0,x1=0.0,x2=0.0;

	if (D<0) cout << "Net deistvitelnih korney\n";

	x=-b/(2*a);
	if (D==0) cout << "Koren odin: x = " << x;

    x1=(-b+D1)/(2*a);
	x2=(-b-D1)/(2*a);
	if (D>0) cout << "Kornia dva: x1 = " << x1 << "; x2 = " << x2;
    
    getch ();

return 0;
}

числа мерсенна
#include "iostream"
#include "conio.h"
#include "cmath"

using namespace std;

int Prime (int a)
{   int sum=0;
	if (a==1) return 0;
	for (int i=2; i<a; i++)
	{	if (a%i!=0) sum++;
		else return 0;
	}
	if (sum==a-2) return 1;
}

void main ()
{   int f, l;
	double n=0, p=0;

    cout << "Enter first element: ";
	cin >> f;

	cout << "Enter last element: ";
	cin >> l;

	cout << "MersPrime:";

	for (int i=f; i<=l; i++)
	{	if (Prime(i))
		{	for(n=0; n<=25; n++)
			{	if (Prime(n))
				{	p=pow(2.0,n)-1;
					if (p==i) cout << " " << i;
				}
			}
		}
	}

	cout << ";";

    getch ();
}

сортировка строк 
int cmp_str (const void *a1, const void *b1)
{	char**a=(char**)a1;
	char**b=(char**)b1;
	return strcmp(*a, *b);
}
qsort(A, n+1, sizeof(char*), cmp_str);

сортировка строк 
#include "iostream"
#include "conio.h"

using namespace std;
int cmp_str (const void *a1, const void *b1)
{	char**a=(char**)a1;
	char**b=(char**)b1;
	return strcmp(*a, *b);
}
void main ()
{	int n;
	char str[100], **A;     //A-массив указателей на char
	
	cout<<"How many words do you want? \n";
	cin>>n;

	A=new char*[n+1];

	cout<<"Input words\n";

	for (int i=0; i<=n; i++)
	{	gets(str);
		A[i]=new char[strlen(str)+1];
		strcpy(A[i], str);
	}

	qsort(A, n+1, sizeof(char*), cmp_str);

	cout<<"Sorted words: \n";

	for(int i=0; i<=n; i++)
	{	puts(A[i]);
	}

	getch();
}

удаление слова и строк
#include "iostream"
#include "conio.h"
#include "cstring"

using namespace std;

char* DelWord(char *str, char *word)
{	int lds1;                                   //lds1 - длина dopstr1; 
	char dopstr1[100], dopstr2[100], *p, *newstr;             // p - указатель на первое вхождение слова в строку. 

	newstr=new char[200];

	p=strstr(str, word);
	
	if (p==0) {cout << "This word doesn`t exist in the string\n"; return 0;}
	else 
	{	strcpy(newstr, str);
		p=strstr(newstr, word);
		while(p)
		{	strcpy(dopstr2, p+strlen(word));

			lds1=strlen(newstr)-strlen(dopstr2)-strlen(word);

			strncpy(dopstr1, newstr, lds1);
			dopstr1[lds1]='\0';

			strcat(dopstr1, dopstr2);

			strcpy(newstr, dopstr1);
			p=strstr(newstr, word);
		}
	}
	return newstr;
}
void main ()
{	char str[100], word[20], *res;

	cout << "Input string \n";
	gets (str);
	cout << "Input word\n";
	gets (word);

	res=DelWord(str, word);

	if(res) puts(res);	

	delete [] res;

	getch ();	
}	

вставка слова в строку
#include "iostream"
#include "conio.h"
#include "cstring"

using namespace std;

char* InsWord(char *str, char *Iword, char *Aword)        //Iword - вставляемое слово, Aword - слово, после которого втсавляется слово, NewStr - новая строка
{	int lIw, lAw, lDS1, lDS2;                                          // lIw - длина вставляемого слова, lAw - длина слова, после которого вставляется, lDS1 - длина dopstr1, lDS2 - длина dopStr2                             
	char *p, *p2, dopstr1[500], dopstr2[300], *newstr;

	newstr=new char[1000];
	
	p=strstr(str, Aword);

	if (p==0) {cout << "This word doesn`t exist in the string\n"; return 0;}
	else 
	{	lAw=strlen(Aword);
		lIw=strlen(Iword);
		strcpy(newstr, str);

		while(p)
		{	strcpy(dopstr2, Aword);
			strcat(dopstr2, Iword);
			strcat(dopstr2, p+strlen(Aword));

			lDS2=strlen(dopstr2);

			lDS1=strlen(newstr)-strlen(p);
		
			strncpy(dopstr1, newstr, lDS1);

			dopstr1[lDS1]='\0';

			strcat(dopstr1, dopstr2);
			strcpy(newstr, dopstr1);

			p2=p+1;
			p=strstr(p2, Aword);	
		}
	}	

	return newstr;
}
void main ()
{	char str[500], Iword[20], Aword[20], *res;

	cout << "Input string:\n";
	gets (str);
	cout << "Input insert word:\n";
	gets (Iword);
	cout << "Input 'after word':\n";
	gets (Aword);

	res=InsWord (str, Iword, Aword);
	if(res) puts(res);	

	getch ();	
}	

замена числа
#include "iostream"
#include "conio.h"
#include "cstring"
#include "stdio.h"

using namespace std;

char* ReplaceNumb(char *str, int Inumb, int Rnumb, char *NewStr)        //Inumb - вставляемое число, Rnumb - число, которое заменяется, NewStr - новая строка
{	int lIw, lRw, lDS1;                                                    // lIw - длина вставляемого слова, lRw - длина слова, которое заменяется, lDS1 - длина dopstr1                              
	char *p, Iword[30], Rword[30], dopstr1[100], dopstr2[100], *newstr;
		
	sprintf(Iword, "%d", Inumb);
	sprintf(Rword, "%d", Rnumb);

	newstr=new char[300];

	p=strstr(str, Rword);

	if (p==0) {cout << "This numb doesn`t exist in the string\n"; return 0;}
	else 
	{	lIw=strlen(Iword);
		lRw=strlen(Rword);
		strcpy(newstr, str);
	
		while(p)
		{	strcpy(dopstr2, Iword);
			strcat(dopstr2, p+lRw);
	
			lDS1=strlen(newstr)-strlen(p);

			strncpy(dopstr1, newstr, lDS1);			
	
			dopstr1[lDS1]='\0';

			strcat(dopstr1, dopstr2);	
			strcpy(newstr, dopstr1);
			p=strstr(newstr, Rword);
		}
	}

	return newstr;
}
void main ()
{	char str[50], NewStr[80], *res;
	int Inumb, Rnumb;

	cout << "Input string:\n";
	gets (str);
	cout << "Input insert word:\n";
	cin >> Inumb;
	cout << "Input 'replace word':\n";
	cin >> Rnumb;

	res=ReplaceNumb (str, Inumb, Rnumb, NewStr);

	if(res) puts(res);	

	delete [] res;

	getch ();	
}	

замена слова
#include "iostream"
#include "conio.h"
#include "cstring"

using namespace std;

char* ReplaceWord(char *str, char *Iword, char *Rword)        //Iword - вставляемое слово, Rword - слово, которое заменяется, NewStr - новая строка
{	int lIw, lRw, lDS1;                                                    // lIw - длина вставляемого слова, lRw - длина слова, которое заменяется, lDS1 - длина dopstr1                              
	char *p, dopstr1[80], dopstr2[30], *newstr;

	newstr=new char[300];
	
	p=strstr(str, Rword);

	if (p==0) {cout << "This word doesn`t exist in the string\n"; return 0;}
	else 
	{	lIw=strlen(Iword);
		lRw=strlen(Rword);
		strcpy(newstr, str);
	
		while(p)
		{	strcpy(dopstr2, Iword);
			strcat(dopstr2, p+lRw);

			lDS1=strlen(newstr)-strlen(p);

			strncpy(dopstr1, newstr, lDS1);

			dopstr1[lDS1]='\0';

			strcat(dopstr1, dopstr2);
			strcpy(newstr, dopstr1);
			p=strstr(newstr, Rword);
		}
	}

	return newstr;
}
void main ()
{	char str[50], Iword[20], Rword[20], *res;

	cout << "Input string:\n";
	gets (str);
	cout << "Input insert word:\n";
	gets (Iword);
	cout << "Input 'replace word':\n";
	gets (Rword);

	res=ReplaceWord (str, Iword, Rword);

	if(res) puts(res);	

	delete [] res;

	getch ();	
}	

массив стэк.h
struct array_stack		/* стек на массиве */
{	int  size;	/* размерность массива */
	int*  p;	/* указатель на массив */
	int  top;	/* верхушка стека */
};
void  init(struct array_stack* s, int size);	// инициализация стека 
void  destruct(struct array_stack* s);	      // разрушение стека 
void  push(struct array_stack* s, int n);	// втолкнуть элемент в стек 
int  pop(struct array_stack* s);	       // вытолкнуть элемент из стека 
int  is_empty(struct array_stack* s);	 // пустой стек? 
int  is_full(struct array_stack* s);		// полный стек? 

массив стэк 
#include "array_stack.h"

void init (struct array_stack* s, int size)                    
{	s->p= new int[size];
	s->size=size;
	s->top=0;
}
void destruct (struct array_stack* s)             
{	delete []s->p;
}
void push (struct array_stack* s, int n)            
{	s->p[s->top]=n;
	s->top++;
}
int pop (struct array_stack* s)                  
{	s->top--;
	return s->p[s->top];
}
int is_empty (struct array_stack* s) 
{	return !(s->top);
}
int is_full (struct array_stack* s)  
{	return s->top==s->size+1;
}

стэк
#include "iostream"
#include "conio.h"
#include "array_stack.h"

using namespace std;

void main ()
{	int a=0;
	array_stack *node = new array_stack;

	cout << "Input size\n";
	cin >> a;

	init (node, a);

	for (int i=a; i>0; i--)
	{	push(node, i);
	}

	if (!(is_full(node)))
	{	for (int i=0; i<a; i++)
		{	cout << pop(node) << " ";
		}
	}

	destruct(node);

	getch();
}

автоморфные числа
#include "iostream"
#include "conio.h"

using namespace std;

int main()
{   long long k=0,a,a2=1,a1,i=1,o,kv;
      
     cout << "Vvedite natural`noe chislo\n";
	 cin >> a1;

	 while (a1<=0)
        {  cout << "Vvedite NATURAL'NOE chislo\n";
           cin >> a1;
        }

	 a=a1;

	 while (a2<=a)
		{  a1=a2; 
		   while (a1>0)
	          {  a1/=10;
	             k++;
	          }

           kv=a2*a2;
	       i=1;

	       for (k; k>0; k--)
	      	  {  i*=10;
	      	  }
	   
	       o=kv%i;
    
           if (o==a2) cout << a2 << " ";
           a2++;
	    }
     
	 cout << "!!!";

	 getch ();

return 0;
}

сумма цифр числа
#include "iostream"
#include "conio.h"

using namespace std;

int main()
{  int x=0,a,b;
    

    cout << "Vvedite natural`noe chislo\n";
	cin >> a;

	while (a<=0)
        {  cout << "Vvedite NATURAL'NOE chislo\n";
           cin >> a;
        }

	while (a>0) 
	    {  x+=a%10;
	       a/=10;
	    }

	cout << "Summa cifr chisla = " << x;

	  getch();

	return 0;
}

сортировка вставками асм
_asm
	{
		mov ecx,n
			dec ecx
			mov edx,a
loop1:
		push ecx;ecx==j
			mov ebx,[edx][ecx*4-4];ebx==z
loop2:
		cmp ecx,n
			jge else1
		cmp [edx][ecx*4],ebx
			jge else1
			
			mov eax,[edx][ecx*4]
		mov [edx][ecx*4-4],eax
			inc ecx
			jmp loop2
else1:
		mov [edx][ecx*4-4],ebx
			pop ecx
			loop loop1
	}

простые числа
int Prime (int a)
{   int sum=0;
	if (a==1) return 0;
	for (int i=2; i<a; i++)
	{	if (a%i!=0) sum++;
		else return 0;
	}
	if (sum==a-2) return 1;
}

выделение памяти под динамическую матрицу
{	a = new int*[col_n];                                     // выделение памяти под массив указателей	
 
	for (int i=0; i<col_n; i++) a[i] = new int[str_n]; 
}

