// ����������� ��������View.h : ��������� ������ C�������������������View
//


#pragma once


class C�������������������View : public CView
{
protected: // ������� ������ �� ������������
	C�������������������View();
	DECLARE_DYNCREATE(C�������������������View)

// ��������
public:
	C�������������������Doc* GetDocument() const;

// ��������
public:

// ���������������
public:
	virtual void OnDraw(CDC* pDC);  // �������������� ��� ��������� ����� �������������
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// ����������
public:
	virtual ~C�������������������View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// ��������� ������� ����� ���������
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // ���������� ������ � ����������� ��������View.cpp
inline C�������������������Doc* C�������������������View::GetDocument() const
   { return reinterpret_cast<C�������������������Doc*>(m_pDocument); }
#endif

