// ����������� ��������View.cpp : ���������� ������ C�������������������View
//

#include "stdafx.h"
#include "����������� ��������.h"

#include "����������� ��������Doc.h"
#include "����������� ��������View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C�������������������View

IMPLEMENT_DYNCREATE(C�������������������View, CView)

BEGIN_MESSAGE_MAP(C�������������������View, CView)
	// ����������� ������� ������
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// ��������/����������� C�������������������View

C�������������������View::C�������������������View()
{
	// TODO: �������� ��� ��������

}

C�������������������View::~C�������������������View()
{
}

BOOL C�������������������View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: �������� ����� Window ��� ����� ����������� ���������
	//  CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// ��������� C�������������������View

void C�������������������View::OnDraw(CDC* /*pDC*/)
{
	C�������������������Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: �������� ����� ��� ��������� ��� ����������� ������
}


// ������ C�������������������View

BOOL C�������������������View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// ���������� �� ���������
	return DoPreparePrinting(pInfo);
}

void C�������������������View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: �������� �������������� ������������� ����� �������
}

void C�������������������View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: �������� ������� ����� ������
}


// ����������� C�������������������View

#ifdef _DEBUG
void C�������������������View::AssertValid() const
{
	CView::AssertValid();
}

void C�������������������View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

C�������������������Doc* C�������������������View::GetDocument() const // �������� ������������ ������
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(C�������������������Doc)));
	return (C�������������������Doc*)m_pDocument;
}
#endif //_DEBUG


// ����������� ��������� C�������������������View
