#include "PBD.h"
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream fin("Input.txt");
	BinTree <int> BT(70);
	int x;
	while(!fin.eof())
	{
		fin >> x;
		BT.add(x);
	}
	//BT.Print();
	cout << BT.n << endl;
	int y=BT.n;
	for(int i=0; i<y; i++)
	    cout << BT.Remove() << endl;

	cout << endl;
	return 0;
}