#include <iostream>
#include <fstream>
#include "FormatText.h"

using namespace std;

Text::Text()
{
	customStringLength = 255;
	data = new char [5000];
	spaces = new char [1000];
	for (int i=0; i<999; i++)
		spaces[i] = ' ';
	spaces[1000] = '\0';
}
Text::Text(int _customStringLength)
{
	customStringLength = _customStringLength;
	data = new char [5000];
	spaces = new char [1000];
	for (int i=0; i<999; i++)
		spaces[i] = ' ';
	spaces[999] = '\0';
}
Text::Text(const Text& T)
{
	customStringLength = T.customStringLength;
	strcpy(data, T.data);
	strcpy(spaces, T.spaces);
}
Text::~Text()
{
	delete [] data;
	delete [] spaces;
}
void Text::FormatText(char *fileName)
{
	ifstream fin (fileName);
	ofstream fout ("Output.txt");

	char** words = new char* [500];

	for (int i=0; i<500; i++)
		words[i] = new char [30];

	fin.getline(data, 5000);
	
	int customCountWords=0; // customCountWords - ����� ���������� ���� � ������

	words[customCountWords] = strtok(data, " ");

	
	while (words[customCountWords++])
		words[customCountWords] = strtok(NULL, " ");

	int stringLength = 0; // ����� ������, ������������ �� ����
	int countWords = 0; // ���������� ����, ������� ������ � ������ ������ customStringLength
	int countWords2 = 0; // ���������������
	int countSpaces = 0; // ������� ���������� ��������
	char resString[255]; // �������������� ������
	resString[0] = '\0'; 
	int lackCountSpaces = 0; 
	int c = 0; // �������
	int k = 0;
	int diffCountWords = countWords - countWords2;

	while (countWords <= customCountWords)
	{
		for (countWords = countWords2; (stringLength + (strlen(words[countWords]) + 1) <= customStringLength) && (countWords < customCountWords); countWords++)
			stringLength += strlen(words[countWords]) + 1;
		stringLength--;

		lackCountSpaces = customStringLength - stringLength; // ���������� ��������, ������ �� ������ ������

		diffCountWords = countWords - countWords2;

		if (lackCountSpaces < diffCountWords)
		{
			for (int i=countWords2; i<countWords; i++)
			{
				strcat (resString, words[i]);

				if (strlen(resString) < customStringLength)
				{
					strncat (resString, spaces, 1);
					
					if (countSpaces < lackCountSpaces)
					{
						strncat(resString, spaces, 1);
						countSpaces++;
					}
				}
			}

			diffCountWords = countWords - countWords2;

			countWords2 = countWords;
			countSpaces = 0;

			fout << resString << endl;
		}
		if (lackCountSpaces > diffCountWords)
		{
			for (int i=countWords2; i<countWords; i++)
			{
				strcat (resString, words[i]);
				
				c = lackCountSpaces / (countWords - countWords2);
				k = lackCountSpaces - (countWords - countWords2)*c + 1;
				if (strlen(resString) < customStringLength)
				{
					strncat (resString, spaces, 1);
					
					if (countSpaces < lackCountSpaces)
					{
						strncat(resString, spaces, c);
						if (k)
							strncat(resString, spaces, 1);
						countSpaces+=c;
						k--;
					}
				}
			}

			diffCountWords = countWords - countWords2;

			countWords2 = countWords;
			countSpaces = 0;

			fout << resString << endl;
		}
		if (lackCountSpaces == diffCountWords)
		{
			for (int i=countWords2; i<countWords; i++)
			{
				strcat (resString, words[i]);

				if (i == countWords2)
					strncat (resString, spaces, 1);
				
				if (strlen(resString) < customStringLength)
					strncat (resString, spaces, 2);
			}

			diffCountWords = countWords - countWords2;

			countWords2 = countWords;
			countSpaces = 0;

			fout << resString << endl;
		}

		stringLength = 0;
		resString[0] = '\0';
	}

	cout << countWords << ' ' << stringLength << endl;																								
}