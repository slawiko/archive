#include "iostream"
#include "conio.h"
#include "array_stack.h"

using namespace std;

void main ()
{	int a=0;
	array_stack *node = new array_stack;

	cout << "Input size\n";
	cin >> a;

	init (node, a);

	for (int i=a; i>0; i--)
	{	push(node, i);
	}

	if (!(is_full(node)))
	{	for (int i=0; i<a; i++)
		{	cout << pop(node) << " ";
		}
	}

	destruct(node);

	getch();
}