struct array_stack		/* ���� �� ������� */
{	int  size;	/* ����������� ������� */
	int*  p;	/* ��������� �� ������ */
	int  top;	/* �������� ����� */
};
void  init(struct array_stack* s, int size);	// ������������� ����� 
void  destruct(struct array_stack* s);	      // ���������� ����� 
void  push(struct array_stack* s, int n);	// ��������� ������� � ���� 
int  pop(struct array_stack* s);	       // ���������� ������� �� ����� 
int  is_pty(struct array_stack* s);	 // ������ ����? 
int  is_full(struct array_stack* s);		// ������ ����? 