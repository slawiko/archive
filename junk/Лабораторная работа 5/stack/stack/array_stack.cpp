#include "array_stack.h"

void init (struct array_stack* s, int size)                    
{	s->p= new int[size];
	s->size=size;
	s->top=0;
}
void destruct (struct array_stack* s)             
{	delete []s->p;
}
void push (struct array_stack* s, int n)            
{	s->p[s->top]=n;
	s->top++;
}
int pop (struct array_stack* s)                  
{	s->top--;
	return s->p[s->top];
}
int is_empty (struct array_stack* s) 
{	return !(s->top);
}
int is_full (struct array_stack* s)  
{	return s->top==s->size+1;
}