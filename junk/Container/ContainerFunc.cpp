#include "Container.h"
#include <iostream>

using namespace std;

ArrayStack::ArrayStack()
{	top=0;
	size=1;
	p = new int [size];
}
ArrayStack::ArrayStack(int size1)
{	size=size1;
	p=new int [size];
	top=0;
}
ArrayStack::ArrayStack(ArrayStack &s)
{	size=s.size;
	top=s.top;
	p = new int [size];
	for (int i=0; i<size; i++)
	{	p[i]=s.p[i];
	}
}
ArrayStack::~ArrayStack()
{	delete [] p;
}
void ArrayStack::push(int &n)
{	if (!IsFull())
	{	p[top++]=n;
	}
	else
		throw FULL();
}
int ArrayStack::pop()
{	if (!IsEmpty())
	{	return p[--top];
	}
	else
		throw EMPTY();
}
bool ArrayStack::IsEmpty() const
{	if (top==0)
		return true;
	return false;
}
bool ArrayStack::IsFull() const
{	if (top==size)
		return true;
	return false;
}
ListNode::ListNode()
{	x=0;
	next=NULL;
}
ListNode::ListNode(int x1)
{	x=x1;
	next=NULL;
}
ListNode::ListNode(int x1, ListNode *next1)
{	x=x1;
	next=next1;
}
ListStack::ListStack()
{	head=NULL;
}
ListStack::ListStack(ListStack& A)
{	ListStack sup;
	int x;
	while(!A.IsEmpty())
	{	x=A.pop();
		sup.push(x);
	}
	head=NULL;
	while(!sup.IsEmpty())
	{	int c=sup.pop();
		push(c);
		A.push(c);
	}
}
void ListStack::print()
	{
		ListNode* temp=head;
		while (temp)
		{
			cout<<temp->x<<" ";
			temp=temp->next;
		}
	}
ListStack::~ListStack()
{	while(!IsEmpty())
		pop();
}
void ListStack::push(int& a)
{	ListNode* s = new ListNode(a,head);
	head=s;
}
int ListStack::pop()
{	if (!IsEmpty())
	{	int sup=head->x;
		ListNode*p=head;
		head=head->next;
		delete p;
		return sup;
	}
	else
		throw EMPTY();
}
bool ListStack::IsEmpty() const
{	return !(head);
}
bool ListStack::IsFull() const 
{	return false;
}
ArrayQueue::ArrayQueue(int _size)
{	size=_size;
	p = new int [size];
	head=0;
	n=0;
}
ArrayQueue::ArrayQueue(ArrayQueue &q)
{	size=q.size;
	n=q.n;
	head=q.head;
	p = new int [size];
	for (int i=0; i<n; i++)
		p[(head+i)%size]=q.p[(q.head+i)%q.size];
	
}
ArrayQueue::~ArrayQueue()
{	delete [] p;
}
void ArrayQueue::push(const int &x)
{	if (!IsFull())
	{	p[(head+n)%size]=x;
		n++;
	}
	else
		throw FULL();
}
void ArrayQueue::print()
{
	for (int i=0,tmp=head;i<n;i++, tmp=(tmp+1)%size)
		cout<<p[tmp]<<" ";
	cout<<endl;
}
int ArrayQueue::pop()
{	if (!IsEmpty())
	{	int a=p[head];
		head= (head+1)%size;
		n--;
		return a;
	}
	else
		throw EMPTY();
}
bool ArrayQueue::IsEmpty() const
{	return (n==0);
}	
bool ArrayQueue::IsFull() const
{	return (size==n);
}
ArrayStackIterator::ArrayStackIterator(ArrayStack &s):
a(s), pos(s.top)
{
}
bool ArrayStackIterator::InRange()
{	return !(pos>=a.size || pos<0);
}
void ArrayStackIterator::Reset()
{	pos=a.top-1;
}
int& ArrayStackIterator::operator *() const
{	return a.p[pos];
}
void ArrayStackIterator::operator ++()
{	pos--;
}
ListStackIterator::ListStackIterator(ListStack &s): a(s), pos(0)
{
}
bool ListStackIterator::InRange()
{	return pos!=NULL;
}
void ListStackIterator::Reset()
{
	pos=a.head;
}
int& ListStackIterator::operator *() const	// ������������� (������ ��������)
	{
		return pos->x;
	}
void ListStackIterator::operator ++()
	{
		pos=pos->next;
	}
ArrayQueueIterator::ArrayQueueIterator(ArrayQueue &_a): a(_a), pos(_a.head)
{
}
bool ArrayQueueIterator::InRange()
{	if ((a.head+a.n)<=a.size)
		return !(pos<a.head || pos>(a.head+a.n-1));
	else
		return !(pos>((a.head+a.n-1)%a.size) && (pos%a.size)<a.head);
}
void ArrayQueueIterator::Reset()
{	pos=a.head;
}
int& ArrayQueueIterator::operator *() const
{	return a.p[pos];
}
void ArrayQueueIterator::operator ++()
{	pos++;
}