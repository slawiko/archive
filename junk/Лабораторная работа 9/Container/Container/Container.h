#include <iostream>
#ifndef SSSSS
#define SSSSS

using namespace std;

//////////////////// ����������� ������ //////////////////////////////
class AbstractContainer						// ����������� ������� ����� ���������
{
public:
	virtual ~AbstractContainer() {};
	virtual bool IsEmpty() const = 0;	// ��������� ����
	virtual bool IsFull()  const = 0;	// ��������� ������
};
class AbstractIterator						// ����������� ������� ����� ��������
{
public:
	virtual ~AbstractIterator() {};
	virtual bool InRange()  = 0;		// ������ � ���������� ��������? 
	virtual void Reset() = 0;			// �������� ������ � ������
	virtual int& operator *() const = 0;// ������������� (������ ��������)
	virtual void operator ++() = 0;		// ����� �� �������
};	

class AbstractStack: public AbstractContainer// ����������� ������� ����� ����
{
public:
	virtual void push(int& n) = 0;		// ��������� � ����
	virtual int pop()  = 0;				// ���������� �� �����
};

class AbstractQueue: public AbstractContainer// ����������� ������� ����� �������
{
public:
	virtual void push(const int& n) = 0;// ��������� � ������� 
	virtual int pop() = 0;				// ������� �� �������
};

//////////////////// ���������� //////////////////////////////////////
struct EMPTY{};
struct FULL{};
//////////////////// ���������� ������ ///////////////////////////////
class StackIterator;		// ������������ ����������
class QueueIterator;		// ������������ ����������
class ArrayStack: public AbstractStack		// ����� ���� �� ���� �������
{
protected:
	int		size;	// ����������� �������
	int*	p;		// ��������� �� ������
	int		top;	// �������� �����
public:
	ArrayStack();
	ArrayStack(int size1);
	ArrayStack(ArrayStack &s);
	virtual ~ArrayStack();
	void push(int& n);				// ��������� � ����
	int pop();						// ���������� �� �����
	bool IsEmpty() const;
	bool IsFull() const ;
	friend class ArrayStackIterator; 
};
class ListNode				// ��������������� ��������� "����"
{
public:
	int x;
	ListNode *next;
    ListNode();
	ListNode(int x1);
	ListNode(int x1, ListNode*next1);
};
class ListStack: public AbstractStack		// ����� ���� �� ���� ������
{	ListNode *head;
public:
	ListStack();
	ListStack(ListStack& A);
	virtual ~ListStack();
	void push (int& a);
	int pop ();
	bool IsEmpty() const;
	bool IsFull() const;
	friend class ListStackIterator;
};

class ArrayQueue: public AbstractQueue			// ����� ��������� �������
{
protected:
	int		size;			// ����������� �������
	int*	p;				// ��������� �� ������
	int		head;			// ������ ������� �������� ��������
	int		n;				// ���������� ��������� � �������
public:
	ArrayQueue(int _size);
	ArrayQueue(ArrayQueue &q);
	~ArrayQueue();
	void push(const int& x);		// ��������� � ������� 
	int pop();
	bool IsEmpty() const;
	bool IsFull() const;
	friend class QueueIterator;
};

// ����� ���
class ArrayDeque: public ArrayQueue
{
public:
	ArrayDeque(int _size);
	ArrayDeque(const ArrayDeque &d);
    ~ArrayDeque();
	void push(const int& x);
	virtual int  pop();					// ���������� �� ���� �� ������� push
	virtual void ins(const int& n);		// �������� � ��� �� ������� del
	bool IsEmpty() const;
	bool IsFull() const;
};
/*
// ����� �������� �����
class ArrayStackIterator: public AbstractIterator
{
	ArrayStack	&a;			// ������ �� ����
	int		pos;			// ������� ������� ���������
	ArrayStackIterator();
public:
	ArrayStackIterator(ArrayStack& );
	bool InRange() ;			// ������ � ���������� ��������
	void Reset();				// �������� ������ � ������
	int& operator *() const;	// ������������� (������ ��������)
	void operator ++();			// ����� �� �������
};

// ����� �������� ����� �� ���� ������
class ListStackIterator: public AbstractIterator
{
};
// ����� �������� ��������� �������
class ArrayQueueIterator: public AbstractIterator
{
	ArrayQueue	&a;			// ������ �� �������
	int		pos;			// ������� ������� ���������
	ArrayQueueIterator();
public:
	ArrayQueueIterator(ArrayQueue& _a);
	bool InRange();				// ������ � ���������� ��������
	void Reset();				// �������� ������ � ������
	int& operator *() const;		// ������������� (������ ��������)
	void operator ++();			// ����� �� �������
};*/
#endif