#include "Container.h"
#include <iostream>

using namespace std;

ArrayStack::ArrayStack()
{	top=0;
	size=1;
	p = new int [size];
}
ArrayStack::ArrayStack(int size1)
{	size=size1;
	p=new int [size];
	top=0;
}
ArrayStack::ArrayStack(ArrayStack &s)
{	size=s.size;
	top=s.top;
	p = new int [size];
	for (int i=0; i<size; i++)
	{	p[i]=s.p[i];
	}
}
ArrayStack::~ArrayStack()
{	delete [] p;
}
void ArrayStack::push(int &n)
{	if (!IsFull())
	{	p[top++]=n;
	}
	else
		throw FULL();
}
int ArrayStack::pop()
{	if (!IsEmpty())
	{	return p[--top];
	}
	else
		throw EMPTY();
}
bool ArrayStack::IsEmpty() const
{	if (top==0)
		return true;
	return false;
}
bool ArrayStack::IsFull() const
{	if (top==size)
		return true;
	return false;
}
ListNode::ListNode()
{	x=0;
	next=NULL;
}
ListNode::ListNode(int x1)
{	x=x1;
	next=NULL;
}
ListNode::ListNode(int x1, ListNode *next1)
{	x=x1;
	next=next1;
}
ListStack::ListStack()
{	head=NULL;
}
ListStack::ListStack(ListStack& A)
{	ListStack sup;
	int x;
	while(!A.IsEmpty())
	{	x=A.pop();
		sup.push(x);
	}
	head=NULL;
	while(!sup.IsEmpty())
	{	int c=sup.pop();
		push(c);
		A.push(c);
	}
}
ListStack::~ListStack()
{	while(!IsEmpty())
		pop();
}
void ListStack::push(int& a)
{	ListNode* s = new ListNode(a,head);
	head=s;
}
int ListStack::pop()
{	if (!IsEmpty())
	{	int sup=head->x;
		ListNode*p=head;
		head=head->next;
		delete p;
		return sup;
	}
	else
		throw EMPTY();
}
bool ListStack::IsEmpty() const
{	return !(head);
}
bool ListStack::IsFull() const 
{	return false;
}
ArrayQueue::ArrayQueue(int _size)
{	size=_size;
	p = new int [size];
	head=-1;
	n=0;
}
ArrayQueue::ArrayQueue(ArrayQueue &q)
{	size=q.size;
	n=q.n;
	head=q.head;
	p = new int [size];
	for (int i=0; i<n; i++)
		p[(head+i)%size]=q.p[(q.head+i)%q.size];
	
}
ArrayQueue::~ArrayQueue()
{	delete [] p;
}
void ArrayQueue::push(const int &x)
{	if (!IsFull())
	{	p[(head+1)%size]=x;
		n++;
		head++;
	}
	else
		throw FULL();
}
int ArrayQueue::pop()
{	if (!IsEmpty())
	{	int a=p[size-n+head];
		head--;
		return a;
	}
	else
		throw EMPTY();
}
bool ArrayQueue::IsEmpty() const
{	return (n==0);
}	
bool ArrayQueue::IsFull() const
{	return (size==n);
}
//ArrayDeque::ArrayDeque(int _size)
//{
//}
//ArrayDeque::ArrayDeque(const ArrayDeque &d)
//{
//}
//ArrayDeque::~ArrayDeque()
//{
//}
//void ArrayDeque::push(const int &x)
//{
//}
//int ArrayDeque::pop()
//{
//}
//void ArrayDeque::ins(const int& n)
//{
//}
//bool ArrayDeque::IsEmpty() const
//{
//}
//bool ArrayDeque::IsFull() const
//{
//}