#include "iostream"
#include "spisok.h"
using namespace std;
void init(spisok** s)
{	(*s)=new spisok;
	(*s)->head=NULL;
}
void insert_head(spisok* s, int a)
{	if (s->head==NULL)
	{	s->head=new spisok::node;
		s->head->item=a;
		s->head->next=NULL;
	}
	else
	{	spisok::node *t=new spisok::node;
		t->item=a;
		t->next=s->head;
		s->head=t;
	}
}
void insert_tail(spisok* s,int a)
{	spisok::node *t=s->head;

	if (t==NULL)
	{	s->head=new spisok::node;
		s->head->item=a;
		s->head->next=NULL;
		return;
	}

	while(t->next!=NULL)
	{	t=t->next;
	}

	t->next=new spisok::node;
	t->next->item=a;
	t->next->next=NULL;
}
int insert_after(spisok* s, int a, int n)
{	spisok::node *t=s->head;

	for(int i=0;i<n;i++)
	{	if (t==NULL) return -1;
		t=t->next;
	}

	spisok::node *p=new spisok::node;
	p->item=a;
	p->next=t->next;
	t->next=p;

	return 1;
}
void output(spisok* s)
{	spisok::node *t=s->head;

	while(t!=NULL)
	{	cout<<t->item<<" ";
		t=t->next;
	}
	cout<<endl;
}
int delete_head(spisok* s)
{	int a=s->head->item;

	spisok::node *t=s->head;
	s->head=s->head->next;

	delete t;
	return a;
}
int delete_tail(spisok* s)
{	spisok::node *t=s->head;

	if(t->next==NULL)
	{	int a=t->item;
		s->head=NULL;
		delete t;
		return a;
	}

	while(t->next->next!=NULL)
	{	t=t->next;
	}

	int a=t->next->item;

	spisok::node *p=t->next;
	delete p;

	t->next=NULL;
	return a;
}

int delete_after(spisok* s,int n)
{	spisok::node *t=s->head;

	for(int i=0;i<n;i++)
	{	t=t->next;
	}

	spisok::node *p=t->next;
	int a=p->item;

	t->next=t->next->next;
	delete p;
	return a;
}
void destruct(spisok* s)
{	spisok::node *t;

	while(s->head!=NULL)
	{	t=s->head;
		s->head=t->next;
		delete t;
	}

	delete s;
}
bool is_empty(spisok* s)
{	return s->head==NULL;
}