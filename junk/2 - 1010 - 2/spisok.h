struct spisok
{	struct node
	{	int item;
		node* next;
	};
	node* head;
};
void init(spisok** s);
void insert_head(spisok* s,int a);
void insert_tail(spisok* s,int a);
int insert_after(spisok* s, int a, int n);
void output(spisok* s);
int delete_head(spisok* s);
int delete_tail(spisok* s);
int delete_after(spisok* s,int n);
void destruct(spisok* s);
bool is_empty(spisok* s);