#include <iostream>
#include <fstream>
#include "1list.h"
using namespace std;
node *head=NULL,*tail=NULL;
void push(int data)
{
	node *tmp=new node;
	tmp->data=data;
	//tmp=tail;
	tmp->next=0;
	if(tail)
		tail->next=tmp;
	else
		head=tmp;
	tail=tmp;
}
int pop()
{
	if(head)
	{
		int rez=head->data;
		node *tmp=new node;
		if(!head->next)
			tail=0;
		head=head->next;
		return rez;
	}
}
void pushindex(int data,int index)
{
	node *tmp=new node;
	tmp->data=data;
	node *point=head;
	int k=0;
	if(!point)
	{	
		tmp->next=0;	
		head=tmp;	
		tail=tmp;		
		return;
	}
	while(k<index&&point->next)
	{
		point=point->next;
		k++;
	}
	if (point->next==0)	
	{ 
		point->next=tmp;	
		tmp->next=0;	
		tail=tmp;		
		return;
	}
	tmp->next=point->next;	
	point->next=tmp;	
}
void deletel()
{
	while(head)
	{
		node *tmp=head;
		head=head->next;
		delete tmp;
	}
	head=NULL;
	tail=NULL;
}
bool is_Empty()
{
	if(head)
		return false;
	else
		return true;
}
void read(char* FILENAME)
{
	ifstream in;
	in.open(FILENAME);
	if(!in.is_open())
		return;
	int data;
	while(!in.eof())
	{
		in>>data;
		push(data);
	}
	in.close();
}
int popindex(int index)
{
	node *tmp=head;
	for(int i=0;i<index;i++)
	{  
		if(!tmp)
			return 0;		 
		tmp=tmp->next;
	}
	int res=tmp->data;
	tmp=tmp->next;
	delete tmp;
	return res;
}

