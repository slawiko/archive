/*��������� �������� �����, ����� ������� � ��������������� ������������� �������. 
�����������, �������� � ������� ������� ������� �������� � �������.*/
#include "iostream"
#include "conio.h"

using namespace std;
int cmp_values(const void *a, const void *b)                       
{   return *(int*)a - *(int*)b;
}
int main()
{   int *a, s=0, mid_ind=0, left_ind=0, right_ind=0, x=0;
    
    cout << "Input the dimension of the array: ";
	cin >> s;

	cout << "Input the required number: ";
	cin >> x;

	a = (int*)malloc(s*sizeof(int));                                // ��������� ������ ��� ������������ ������

    cout << "Input elements of the array \n";
	for (int i=1; i<=s; ++i) cin >> a[i];

	cout << "Your array is: {";
	for (int k=1; k<=s; k++) cout << " " << a[k] ;
	cout << " }\n";
	
	qsort(a, s+1, sizeof(int), cmp_values);                         // ����������

	cout << "Your sorted array is: {";                              
	for (int k=1; k<=s; k++) cout << " " << a[k] ;
	cout << " }\n";

	left_ind=1;
	right_ind=s;

	if (x>a[right_ind]) 
	{   cout << "The required number isn`t in the array\n";         // �������� �� �������������� ���������
	    getch (); 
		return 0;
	}

    if (x<a[left_ind]) 
	{   cout << "The required number isn`t in the array\n"; 
	    getch (); 
		return 0;
	}

    while (left_ind<right_ind)                                           // ���� ������ ��������            
    {   mid_ind = left_ind + (right_ind - left_ind)/2;

   	    if (x>a[mid_ind]) left_ind = mid_ind+1;
		else
	    if (x<a[mid_ind]) right_ind = mid_ind-1;
		else
		{   cout << "Index of the required element: " << mid_ind; 
		    getch (); 
			return 0;
		}
	}

	cout << "The required number isn`t in the array\n"; 
	getch (); 
	return 0;	
} 
