/*������������� ������������� ������ �������� �������, ������� ��� ������.
����� ���� ���������� ����������� � �������. ����������� � �������� ������� �������� � �������.*/

#include "iostream"
#include "conio.h"

using namespace std;

int Sort_Ins (int s, int *a);
int Sort_Sel (int s, int *a);
int Sort_Bub (int s, int *a);
int main ()
{   int *a, s=0, type=0, k=1;

    cout << "Input the dimension of the array: ";
	cin >> s;
	
	cout << "Select the type of sort\nEnter 1 - Insertion sort, 2 - Selection sort, 3 - Bubble sort\n";
	cin >> type;

    while (type>3 || type<1)
    	{   cout << "Reselect the type of sort\nEnter 1 - Insertion sort, 2 - Selection sort, 3 - Bubble sort\n";
	        cin >> type;
	    }

	if (type==1) cout << "Insertion sort\n";
	if (type==2) cout << "Selection sort\n";
	if (type==3) cout << "Bubble sort\n";	

	a = (int*)malloc(s*sizeof(int));                                          // ��������� ������ ��� ������������ ������

    cout << "Input elements of the array \n";                                 // ���� ��������� ������� � �������
	for (int i=1; i<=s; ++i) cin >> a[i];

	if (type==1) Sort_Ins (s, a);
	if (type==2) Sort_Sel (s, a);                                             // ����� ���� ����������
	if (type==3) Sort_Bub (s, a);	
	
	cout << "Your sorted array is: {";                              
	for (int k=1; k<=s; k++) cout << " " << a[k];                             // ����� ���������������� �������
	cout << " }\n";

	getch();
	return 0;
}
int Sort_Ins (int s, int *a)                                                  // ������� ���������� ���������
{   for(int i=1; i<=s; i++)      
    {   for(int j=i; j>0 && a[j-1]>a[j]; j--) swap(a[j-1],a[j]);         
    }
    return *a;
}
int Sort_Sel (int s, int *a)                                                  // ������� ���������� �������
{   int j=1, min=0;
    for (int i=0; i<s-1; i++)
	{   min=i;
		for (j=i+1; j<=s; j++)
		{   if (a[j]<a[min]) min=j;
	    }
		if (min!=i) swap (a[i], a[min]);
	}
	return *a;
}
int Sort_Bub (int s, int *a)                                                  // ������� ���������� ���������
{   for (int ind=0; ind<s-1; ind++)   
	{   for (int k=1; k<s; k++) 
        {   if (a[k]>a[k+1]) swap (a[k], a[k+1]);	
        }
	}
	return *a;
}
