#ifndef SSSS
#define SSSS

#include <iostream>

using namespace std;

const int root = 2000000000;//T

class Tree
{
	int* heap;//T
	int count;
	int size;
public:
	Tree()
	{
		size = 10;
		heap = new int [size+1];//T
		count = 0;
		heap[0] = root;
	}
	Tree(int _size)
	{
		size = _size;
		heap = new int [size+1];//T
		count = 0;
		heap[0] = root;
	}
	Tree(const Tree& A)
	{
		size = A.size;
		count = A.count;
		heap = new int [size+1];//T
		heap[0] = root;
		for (int i=1; i<count; i++)
			heap[i] = A.heap[i];
	}
	virtual ~Tree()
	{
		delete [] heap;
	}
	void reBuild (int index)
	{
		int resultIndex = index;

		if (2*index <= count && heap[2*index] > heap[index]) // ���� �� ����� �������
			resultIndex = 2*index;

		if (2*index+1 <= count && heap[2*index+1] > heap[resultIndex]) // ���� �� ������ �������
			resultIndex = 2*index+1;

		if (resultIndex == index) // ���� ��� ����
			return;
		
		int x = heap[index];//T
		heap[index] = heap[resultIndex];
		heap[resultIndex] = x;

		reBuild (resultIndex);
	}
	int remove()//T
	{
		int x=heap[1];//T
		heap[1] = heap[count];
		reBuild(1);
		return x;
	}
	void add (int& x)//T
	{
		heap[++count] = x;

		int resultIndex = count;
		while (heap[resultIndex] > heap[resultIndex/2])
		{
			int x = heap[resultIndex];//T
			heap[resultIndex] = heap[resultIndex/2];
			heap[resultIndex/2] = x;

			resultIndex = resultIndex/2;
		}
	}

	


};
#endif 