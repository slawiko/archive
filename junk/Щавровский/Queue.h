#include <iostream>
#include <fstream>
#ifndef SSSSS
#define SSSSS

using namespace std;

struct node
{	int x, y;
	node* next;
public:
	node ();
	node (int x1, int y1, node *next1);
	const node &  operator = (const node& B);
};

class Queue
{	node *head;
	node *tail;
public:
	Queue ();
	Queue(const Queue &A);
	virtual ~Queue();
	bool push (int x1, int y1);
	node pop ();
	bool is_empty ();
	void print (ostream &out);
};
#endif