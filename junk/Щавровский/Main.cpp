#include <iostream>
#include "Queue.h"

using namespace std;

int main ()
{
	node hod[8], Z;
	ifstream fin("Input.txt");
	ofstream fout("Output.txt");
	char k[1000];
	int time=0;
	int iterator=0;
	int debug1, debug2;
	char answer[100];

	hod[0].x=1;  //������
	hod[0].y=0;  

	hod[1].x=-1; //�����
	hod[1].y=0;	 

	hod[2].x=0;	 //�����	
	hod[2].y=-1; 

	hod[3].x=0;	 //����
	hod[3].y=1;	 

	int N, M; // ����������� �����
	int xs, ys; // ���������� ������
	int xf, yf; // ���������� ������

	fin >> N;
	fin >> M;
	fin >> xs;
	fin >> ys;
	fin >> xf;
	fin >> yf;

	char c[2];
	fin.getline(c,2);


	char **map = new char *[N];
	bool flag = false;	

	for (int i=0; i<N; i++)
	{	
		map[i] = new char [M];
	}
	
	while (!fin.eof())
		for (int i=0; i<N; i++)
		{	
			fin.getline(k, M+1);
			for (int j=0; j<M; j++)
				map[i][j]=k[j];
		}

	char **map_2 = new char *[N];

	for (int i=0; i<N; i++)
	{	
		map_2[i] = new char [M];
	}
	
	for (int i=0; i<N; i++)
		for (int j=0; j<M; j++)
			map_2[i][j]=map[i][j];

	node *exit = new node [M*N];
	Queue Q;

	if (map[ys-1][xs-1] == '.')
		time+=1;
	else
		time+=2;

	Q.push(xs-1,ys-1);
	map[ys-1][xs-1]='4';

	while (!Q.is_empty())
	{	
		Z = Q.pop();
		if (Z.x==xf-1 && Z.y==yf-1)
		{	
			flag=true;
			break;
		}
		for (int i=0; i<4; i++)
		{	
			int yn, xn;
			debug1=Z.y + hod[i].y;
			debug2=Z.x + hod[i].x;

			if ((debug1 >= 0 && debug2 >= 0) && (debug1 < N && debug2 < M))
			{
				yn=Z.y + hod[i].y;
				xn=Z.x + hod[i].x;
			}

			if (map[yn][xn]=='.')
			{	
				Q.push(xn,yn);
				map[yn][xn]=i + '0';
			}
			if (map[yn][xn]=='W')
			{
				Q.push(xn,yn);
				map[yn][xn]=i + '0';
			}
		}
	}

	for (int i=0; i<N; i++)
	{	
		for (int j=0; j<M; j++)
			cout << map[i][j];
		cout << endl;
	}
	
	cout << endl;

	for (int i=0; i<N; i++)
	{	
		for (int j=0; j<M; j++)
			cout << map_2[i][j];
		cout << endl;
	}


	int j=0;

	while (map[Z.y][Z.x]!='4')
	{	char C = map [Z.y][Z.x];
		int q = C - '0';
		Z.y-=hod[q].y;
		Z.x-=hod[q].x;

		if (map_2[Z.y][Z.x] == 'W')
			time+=2;
		else
			time+=1;

		switch (q)
		{
			case 0:
				answer[iterator++] = 'E';
				break;
			case 1:
				answer[iterator++] = 'W';
				break;
			case 2:
				answer[iterator++] = 'N';
				break;
			case 3:
				answer[iterator++] = 'S';
				break;
		}
	}

	answer[iterator] = '\0';

	fout << "Way: " << answer << '\n';
	fout << "Time: " << time << '\n';
	cout << "Way: " << answer << endl;
	cout << "Time: " << time << endl;

	return 0;
}