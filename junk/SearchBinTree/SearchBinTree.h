#ifndef SSSS
#define SSSS

#include <iostream>

using namespace std;
//����� ����� ������ ������
class Tree
{
private:
	struct TreeNode
	{	
		int key;
		void* data;
		TreeNode* left;
		TreeNode* right;
	};
	TreeNode* root;
public:
	TreeNode*& findNode (TreeNode*& node, int _key);
//	void removeNode (TreeNode*& p);
//	bool add (int key, void* data);
};
class Student
{	
	char name[15];
	int group;
	double grade;
public:
	Student();
	Student(char* _name, int _group, double _grade);
	void printStudent();
};

class StudentTree: public Tree
{
public:
	virtual void print(void* A);
};
#endif 