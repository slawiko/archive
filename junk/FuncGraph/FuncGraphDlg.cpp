// FuncGraphDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "FuncGraph.h"
#include "FuncGraphDlg.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// ���������� ���� CFuncGraphDlg




CFuncGraphDlg::CFuncGraphDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFuncGraphDlg::IDD, pParent)
{
	size = 200;								//���������� ��������� �������
	mass = new Point[size];					//���������� �� ������ �����
	koef_m = 1.;							//����������� ���������������
	minx = 0.;								//����������� �
	miny = 0.;								//����������� �
	maxx = 0.;								//������������ �
	maxy = 0.;								//������������ �
	vsizex = 0;								//����������� �������
	vsizey = 0;								//����������� �������
	flag=false;

	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFuncGraphDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFuncGraphDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CFuncGraphDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CFuncGraphDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CFuncGraphDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CFuncGraphDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDCANCEL, &CFuncGraphDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CFuncGraphDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// ����������� ��������� CFuncGraphDlg

BOOL CFuncGraphDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// ���������� ������ ''� ���������...'' � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CFuncGraphDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ������.

void CFuncGraphDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this); // device context for painting
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width());    // ����� /2
		int y = (rect.Height()); // ����� /2
		CPen* old;

		if (flag)
		{
			CPen pen(PS_SOLID,2,RGB(0,0,0));
			old = dc.SelectObject (&pen);
			dc.MoveTo(0,maxy*koef_m + 10);
			dc.LineTo(x,maxy*koef_m + 10);
			dc.MoveTo((-minx)*koef_m + 10,y);
			dc.LineTo((-minx)*koef_m + 10,0);
			dc.MoveTo(x,maxy*koef_m + 10);		// ������� x
			dc.LineTo(x-5,maxy*koef_m + 5);		// ������� x
			dc.MoveTo(x,maxy*koef_m + 10);		// ������� x
			dc.LineTo(x-5,maxy*koef_m + 15);	// ������� x
			dc.MoveTo((-minx)*koef_m + 10,0);	// ������� y
			dc.LineTo((-minx)*koef_m + 5,5);	// ������� y
			dc.MoveTo((-minx)*koef_m + 10,0);	// ������� y
			dc.LineTo((-minx)*koef_m + 15,5);	// c������ y
			//dc.MoveTo();   // �������

			CPen pen2(PS_SOLID,4,RGB(255,0,0));
			old = dc.SelectObject (&pen2);
			int newx, newy, newx2, newy2;
			for (int i=0;i<size-1;i++)
			{
				int newx = (int)((mass[i].x - minx)*koef_m + 10); 
				int newy = (int)((maxy - mass[i].y)*koef_m + 10);
				dc.MoveTo(newx,newy);

				int newx2 = (int)((mass[i+1].x - minx)*koef_m + 10);
				int newy2 = (int)((maxy - mass[i+1].y)*koef_m + 10);
				dc.LineTo(newx2,newy2);
			}
			flag = true;
		}
		else
		{
			CPen pen(PS_SOLID,1,RGB(0,0,0));
			old = dc.SelectObject (&pen);
		}

	dc.SelectObject(*old);			
	CDialog::OnPaint();
	}
}

void CFuncGraphDlg::Koef ()
{	
	minx = mass[0].x;
	maxx = mass[size-1].x;

	if (0<minx)
		minx = 0;
	if (0>maxx)
		maxx = 0;

	CRect rect;
	GetClientRect(&rect);
	vsizex=rect.Width();
	vsizey=rect.Height();

	double koef_mx = (vsizex-20)/(maxx-minx);

	miny = mass[0].y;
	maxy = mass[0].y;
	for (int i=0; i<size; i++)
	{	if (mass[i].y>maxy)
			maxy = mass[i].y;
		if (mass[i].y<miny)
			miny = mass[i].y;
	}
		
	if (0<miny)
		miny = 0;
	if (0>maxy)
		maxy = 0;

	double koef_my = (vsizey-20)/(maxy-miny);

	if (koef_my<koef_mx)
		koef_m = koef_my;
	else
		koef_m = koef_mx;
}
void CFuncGraphDlg::CalcSin()
{	
	double x1=-5.0, y1;

	for (int i=0; i<size; i++)
	{	
		x1+=0.05;    // 10/200
		y1=sin(x1);
		mass[i].x=x1;
		mass[i].y=y1;
	}
}
void CFuncGraphDlg::CalcPar()
{	
	double x1=-5.0, y1;

	for (int i=0; i<size; i++)
	{
		x1+=0.05;   // 10/200
		y1=x1*x1;
		mass[i].x=x1;
		mass[i].y=y1;
	}
}
void CFuncGraphDlg::CalcHyp()
{
	double x1=-5.0, y1;

	for (int i=0; i<size; i++)
	{	
		x1+=0.05;    // 10/200
		y1=(x1*x1*x1)/10;
		mass[i].x=x1;
		mass[i].y=y1;
	}
}
void CFuncGraphDlg::CalcCos()
{
	double x1=-5.0, y1;

	for (int i=0; i<size; i++)
	{	
		x1+=0.05;    // 10/200
		y1=cos(x1);
		mass[i].x=x1;
		mass[i].y=y1;
	}
}
// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CFuncGraphDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFuncGraphDlg::OnBnClickedButton1()
{
	flag = true;
	CalcSin();
	Koef();

	Invalidate();// TODO: �������� ���� ��� ����������� �����������
}

void CFuncGraphDlg::OnBnClickedButton2()
{
	flag = true;
	CalcPar();
	Koef();

	Invalidate();// TODO: �������� ���� ��� ����������� �����������
}

void CFuncGraphDlg::OnBnClickedButton3()
{
	flag = true;
	CalcHyp();
	Koef();

	Invalidate();// TODO: �������� ���� ��� ����������� �����������
}

void CFuncGraphDlg::OnBnClickedButton4()
{
	flag = true;
	CalcCos();
	Koef();

	Invalidate();// TODO: �������� ���� ��� ����������� �����������
}

void CFuncGraphDlg::OnBnClickedCancel()
{
	// TODO: �������� ���� ��� ����������� �����������
	flag = false;

	Invalidate();
}

void CFuncGraphDlg::OnBnClickedOk()
{
	// TODO: �������� ���� ��� ����������� �����������
	OnOK();
}
