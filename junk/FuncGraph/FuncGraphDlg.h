// FuncGraphDlg.h : ���� ���������
//

#pragma once


// ���������� ���� CFuncGraphDlg
struct Point
{	double x, y;
	
	Point(): x(0.), y(0.){}
	Point(double x1, double y1): x(x1), y(y1){}
};
class CFuncGraphDlg : public CDialog
{
// ��������
public:
	CFuncGraphDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_FUNCGRAPH_DIALOG };

	Point* mass;
	int size, vsizex, vsizey;  // size - ���������� �����, 
	double koef_m, minx, miny, maxx, maxy;
	bool flag;

	void Koef ();
	void CalcSin ();
	void CalcPar ();
	void CalcHyp ();
	void CalcCos ();

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedOk();
};
