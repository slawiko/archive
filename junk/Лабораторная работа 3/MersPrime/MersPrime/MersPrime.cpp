/*������� �� ������� ��� ����� ������� � �������� ���������. 
������ ������� ���������� ������� ����� n, ������� ����������� 
� ���� (2^p)-1, ��� � � ����� ������� �����. ��� ������� ������ 
������������ ������� ��� �����������, �������� �� �������� 
����������� ����� �������.*/

#include "iostream"
#include "conio.h"
#include "cmath"

using namespace std;

int Prime (int a)
{   int sum=0;
	if (a==1) return 0;
	for (int i=2; i<a; i++)
	{	if (a%i!=0) sum++;
		else return 0;
	}
	if (sum==a-2) return 1;
}

void main ()
{   int f, l;
	double n=0, p=0;

    cout << "Enter first element: ";
	cin >> f;

	cout << "Enter last element: ";
	cin >> l;

	cout << "MersPrime:";

	for (int i=f; i<=l; i++)
	{	if (Prime(i))
		{	for(n=0; n<=25; n++)
			{	if (Prime(n))
				{	p=pow(2.0,n)-1;
					if (p==i) cout << " " << i;
				}
			}
		}
	}

	cout << ";";

    getch ();
}